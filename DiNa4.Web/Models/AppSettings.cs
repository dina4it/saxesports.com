﻿namespace DiNa4.Web {
    public class AppSettings {
        public const string EXTERNAL_REFERER = "EXT-REFERER";
        public GoogleRecaptcha GoogleRecaptcha { get; set; }
        public BoardSettings BoardSettings { get; set; }
        public MailSettings MailSettings { get; set; }
        public NConvert NConvert { get; set; }
        public string GDPR_ID { get; set; }
        public bool AllowLanguageRedirect { get; set; }
        public bool IsLocal { get; set; }
        public bool GlobalErrorHandler { get; set; }
        ////public static int Umbraco_PrivacyTemplateId = int.Parse(ConfigurationManager.AppSettings["Umbraco_PrivacyTemplateId"]);
        //public static string GDPR_URI = ConfigurationManager.AppSettings["GDPR_URI"];
        //public static bool BetaAuth_Enabled = bool.Parse(ConfigurationManager.AppSettings["BetaAuth_Enabled"]);

    }


    public class GoogleRecaptcha {
        public bool Activate { get; set; }
        public string PrivateKey { get; set; }
        public string PublicKey { get; set; }
    }

    public class BoardSettings {
        public int WebsiteId { get; set; }
        public int SupercompanyId { get; set; }
        public bool AllowDashboard { get; set; }
        public bool AllowPmsImport { get; set; }
    }

    public class MailSettings {
        public string EmailAdmin { get; set; }
        public string EmailFrom { get; set; }
        public string EmailErrorFrom { get; set; }
        public string EmailFromName { get; set; }
        public string Email_Contact { get; set; }
        public string Hostname { get; set; }
    }

    public class NConvert {
        public string MaxWidth { get; set; }
        public string MaxHeight { get; set; }
        public string Quality { get; set; }
        public bool Activate { get; set; }
    }
}