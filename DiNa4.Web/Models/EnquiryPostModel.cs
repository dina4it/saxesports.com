﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Http;

namespace Dina4.Web.Models {
    public class EnquiryPostModel {
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? Category { get; set; }
        public string CategoryName { get; set; }
        public List<int> SubCategory { get; set; }
        public List<string> SubCategoryName { get; set; }
        public string Altsubcategory { get; set; }
        public string PrimaryColor { get; set; }
        public List<string> Colors { get; set; }
        public List<string> Introductions { get; set; }

        public IFormFile Logo1 { get; set; }
        public IFormFile Logo2 { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Company { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Title { get; set; }

        public int? Adults { get; set; }
        public int? Children { get; set; }
        public List<int> ChildrenAges { get; set; }

        public string Arrival { get; set; }
        public string Departure { get; set; }
        public int Nights { get; set; }

        public string Room { get; set; }
        public string Offer { get; set; }
        public List<string> Interests { get; set; }

        public string Message { get; set; }
        public string Ip { get; set; }
        public string Referer { get; set; }
        public int LanguageId { get; set; }
    }
}