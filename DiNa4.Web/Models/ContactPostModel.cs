﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dina4.Web.Models {
    public class ContactPostModel {
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public string Adults { get; set; }
        public string Children { get; set; }
        public List<int> ChildrenAge { get; set; }

        public string Arrival { get; set; }
        public string Departure { get; set; }
        public string Nights { get; set; }

        public string Message { get; set; }
        public int LanguageId { get; set; }
    }
}