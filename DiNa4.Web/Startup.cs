using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using DiNa4.Web.DataService;
using DiNa4.Web.DB;
using Dina4.Web.Extensions;
using Dina4.Web.Helpers;
using DiNa4.Web.Helpers;
using DiNa4.Web.Middleware;
using DiNa4.Web.Notifications;
using Dina4.Web.src;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Org.BouncyCastle.Asn1.Ocsp;
using Umbraco.Cms.Core.Configuration.Models;
using Umbraco.Cms.Core.DependencyInjection;
using Umbraco.Cms.Core.Notifications;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Core.Strings;
using Umbraco.Cms.Core.Web;
using Umbraco.Extensions;
using WebMarkupMin.AspNet.Common.UrlMatchers;
using WebMarkupMin.AspNetCore6;
using WebMarkupMin.Core;
using UrlHelperExtensions = Umbraco.Cms.Web.BackOffice.Trees.UrlHelperExtensions;

namespace DiNa4.Web {
    public class Startup {
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _config;

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup" /> class.
        /// </summary>
        /// <param name="webHostEnvironment">The web hosting environment.</param>
        /// <param name="config">The configuration.</param>
        /// <remarks>
        /// Only a few services are possible to be injected here https://github.com/dotnet/aspnetcore/issues/9337
        /// </remarks>
        public Startup(IWebHostEnvironment webHostEnvironment, IConfiguration config) {
            _env = webHostEnvironment ?? throw new ArgumentNullException(nameof(webHostEnvironment));
            _config = config ?? throw new ArgumentNullException(nameof(config));
        }

        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <remarks>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        /// </remarks>
        public void ConfigureServices(IServiceCollection services) {
#pragma warning disable IDE0022 // Use expression body for methods

            // Html Minify
            services.AddWebMarkupMin(
                    options => {
                        options.AllowMinificationInDevelopmentEnvironment = true;
                        options.AllowCompressionInDevelopmentEnvironment = true;
                    })
                .AddHtmlMinification(
                    options => {
                        options.ExcludedPages = new List<IUrlMatcher> {
                            new WildcardUrlMatcher("/umbraco/*"),
                            //new WildcardUrlMatcher("/admin/*"),
                            //new ExactUrlMatcher("/admin"),
                        };
                        //options.MinificationSettings.RemoveRedundantAttributes = true;
                        options.MinificationSettings.RemoveHttpProtocolFromAttributes = false;
                        options.MinificationSettings.RemoveHttpsProtocolFromAttributes = false;
                        options.MinificationSettings.AttributeQuotesRemovalMode = HtmlAttributeQuotesRemovalMode.KeepQuotes;
                        options.MinificationSettings.RemoveHtmlComments = true;
                        options.MinificationSettings.WhitespaceMinificationMode = WhitespaceMinificationMode.Safe;
                    })
                .AddHttpCompression();

            services
                .AddUmbraco(_env, _config)
                .AddBackOffice()
                .AddWebsite()
                .AddComposers()
                .SetContentLastChanceFinder<My404ContentFinder>()
                .AddNotificationHandler<MediaSavingNotification, ImagePreprocessingHandler>()
                .AddNotificationHandler<ContentPublishedNotification, ContentPublishedNotificationHandler>()
                .Build();
#pragma warning restore IDE0022 // Use expression body for methods

            services.Configure<AppSettings>(_config.GetSection("DiNa4"));
            //LanguageRedirect.AllLanguages = umbracoContextAccessor.GetRequiredUmbracoContext().Domains.GetAll(false).ToList();
            //LanguageRedirect.AllCultures = LanguageRedirect.AllLanguages.Select(_ => _.Culture).ToList();



            // Entity Framework
            services.AddDbContext<DiNa4DataContext>(
                options => options.UseSqlServer("name=ConnectionStrings:umbracoDbDSN"));

            //services.AddDatabaseDeveloperPageExceptionFilter();

            //UrlHelper

            //services.Configure()
            //services.AddSingleton<IActionContextAccessor, ActionContextAccessor>()
            //    .AddScoped<IUrlHelper>(x => x
            //        .GetRequiredService<IUrlHelperFactory>()
            //        .GetUrlHelper(x.GetRequiredService<IActionContextAccessor>().ActionContext));
        }

        /// <summary>
        /// Configures the application.
        /// </summary>
        /// <param name="app">The application builder.</param>
        /// <param name="env">The web hosting environment.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
            ILocalizationService localizationService,
            IOptions<AppSettings> appSettings,
            IOptions<GlobalSettings> globalSettings,
            IShortStringHelper shortStringHelper,
            IHttpContextAccessor httpContextAccessor,
            ILogger<Startup> logger,
            IWebHostEnvironment hostEnvironment) {
            AppDomain.CurrentDomain.SetData("ContentRootPath", env.ContentRootPath);
            AppDomain.CurrentDomain.SetData("WebRootPath", env.WebRootPath);

            // Configure Extensions
            UrlHelperExtensions.Configure(httpContextAccessor);
            HtmlHelperExtensions.Configure(httpContextAccessor);

            //// Add Rewrites
            //using (StreamReader iisUrlRewriteStreamReader = File.OpenText("IISUrlRewrite.xml")) {
            //    var options = new RewriteOptions()
            //        //.AddRedirect("redirect-rule/(.*)", "redirected/$1")
            //        //.AddRewrite(@"^rewrite-rule/(\d+)/(\d+)", "rewritten?var1=$1&var2=$2",
            //        //skipRemainingRules: true)
            //        //.AddApacheModRewrite(apacheModRewriteStreamReader)
            //        //.Add(MethodRules.RedirectXmlFileRequests)
            //        //.Add(MethodRules.RewriteTextFileRequests)
            //        //.Add(new RedirectImageRequests(".png", "/png-images"))
            //        //.Add(new RedirectImageRequests(".jpg", "/jpg-images"))
            //        .AddIISUrlRewrite(iisUrlRewriteStreamReader); ;

            //    app.UseRewriter(options);
            //}

            // Exceptionhandler
            app.UseExceptionHandler(options => {
                options.Run(async context => {
                    //context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    //context.Response.ContentType = "text/html";

                    try {
                        if (appSettings?.Value.GlobalErrorHandler == true) {
                            var exceptionObject = context.Features.Get<IExceptionHandlerFeature>();
                            if (null != exceptionObject) {
                                var errorMessage = $"{exceptionObject.Error.Message}";
                                if (!context.Request.GetDisplayUrl().ContainsAny(new string[] { ".jpg", ".jpeg", ".png", "Illegal characters in path" })) {
                                    //MailHelper.SendMail(AppSettings.EmailError, "admin@dina4.it", Request.Url.Host,
                                    //    "ERROR: " + Request.Url.Host,
                                    //    HttpContext.Current.Request.Url.AbsoluteUri + "<br>" + exception.Message + "<br>" +
                                    //    exception.StackTrace);
                                    try {
                                        var mailbody = context.Request.GetDisplayUrl() + "<br>" + exceptionObject.Error.Message + "<br>" +
                                                       exceptionObject.Error.StackTrace;
                                        MailHelper.SendErrorMail("ERROR: " + context.Request.Host, mailbody);
                                    } catch (Exception ex) {
                                        logger.LogError(ex, ex.Message);
                                    }
                                }
                                //await context.Response.WriteAsync(errorMessage).ConfigureAwait(false);

                            }
                        }
                    } catch (Exception ex) {
                    }
                });
            });

            //if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            //}

            app.UseForwardedHeaders(new ForwardedHeadersOptions {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor |
                                   ForwardedHeaders.XForwardedProto
            });

            app.UseWebMarkupMin();

            app.UseUmbraco()
                .WithMiddleware(u => {
                    u.UseLanguageRedirectMiddleware();
                    u.UseExternalReferrerParserMiddleware();
                    u.UseBackOffice();
                    u.UseWebsite();
                })
                .WithEndpoints(u => {
                    u.EndpointRouteBuilder.MapControllerRoute("Json Controller", "/api/json/{action}", new { Controller = "Json" });
                    u.EndpointRouteBuilder.MapControllerRoute("DictionaryHelper Controller", "/api/dictionary/{action}", new { Controller = "DictionaryHelper" });
                    u.UseInstallerEndpoints();
                    u.UseBackOfficeEndpoints();
                    u.UseWebsiteEndpoints();
                });

            // Tranlsation Static Helper set localizationService
            T.Configure(localizationService, hostEnvironment);
            MailHelper.Configure(appSettings, globalSettings);
            WebsiteUrlHelper.Configure(shortStringHelper);
        }
    }
}
