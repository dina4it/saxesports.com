﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Dina4.EnquiryApi;
using Dina4.EnquiryApi.DB;
using DiNa4.Web;
using DiNa4.Web.DB;
using DiNa4.Web.Extensions;
using DiNa4.Web.Helpers;
using Dina4.Web.Models;
using Dina4.Web.src;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Umbraco.Cms.Core.Configuration.Models;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web;
using Umbraco.Cms.Web.Common.Controllers;

namespace DiNa4.Web.Controllers {
    // Musste DictionaryHelper genannt werden, da er sonst in Konflikt mit Umbraco kommt
    public class DictionaryHelperController : UmbracoPageController {
        private readonly IOptions<GlobalSettings> _globalSettings;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly DiNa4DataContext _diNa4DataContext;
        private readonly IConfiguration _configuration;

        public DictionaryHelperController(ILogger<UmbracoPageController> logger, ICompositeViewEngine compositeViewEngine, IOptions<GlobalSettings> globalSettings, IOptions<AppSettings> appSettings,
            DiNa4DataContext diNa4DataContext, IConfiguration configuration)
            : base(logger, compositeViewEngine) {
            _appSettings = appSettings;
            _diNa4DataContext = diNa4DataContext;
            _configuration = configuration;
            _globalSettings = globalSettings;
        }
        public IActionResult Export() {
            var export = T.ExportAllToCsv();
            byte[] bytes = Encoding.UTF8.GetBytes(export);
            bytes = Encoding.UTF8.GetPreamble().Concat(bytes).ToArray();
            return File(bytes, "application/octet-stream; charset=utf-8", "ExportDictionary.csv");
        }

        public ActionResult Import() {
            var result = T.ImportCsv();
            return Content(result.ToString());
        }
    }
}