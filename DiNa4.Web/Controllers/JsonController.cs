﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Dina4.EnquiryApi;
using Dina4.EnquiryApi.DB;
using DiNa4.Web;
using DiNa4.Web.DB;
using DiNa4.Web.Extensions;
using DiNa4.Web.Helpers;
using Dina4.Web.Models;
using Dina4.Web.src;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Umbraco.Cms.Core.Configuration.Models;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web;
using Umbraco.Cms.Web.Common.Controllers;
using Umbraco.Cms.Web.Common.Filters;
using Umbraco.Cms.Web.Common.PublishedModels;
using Umbraco.Extensions;

namespace DiNa4.Web.Controllers {
    public class JsonController : UmbracoPageController {
        private readonly IOptions<GlobalSettings> _globalSettings;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly DiNa4DataContext _diNa4DataContext;
        private readonly IConfiguration _configuration;
        private readonly IUmbracoContextFactory _umbracoContextFactory;

        public JsonController(ILogger<UmbracoPageController> logger, ICompositeViewEngine compositeViewEngine, IOptions<GlobalSettings> globalSettings, IOptions<AppSettings> appSettings,
            DiNa4DataContext diNa4DataContext, IConfiguration configuration, IUmbracoContextFactory umbracoContextFactory)
            : base(logger, compositeViewEngine) {
            _appSettings = appSettings;
            _diNa4DataContext = diNa4DataContext;
            _configuration = configuration;
            _umbracoContextFactory = umbracoContextFactory;
            _globalSettings = globalSettings;
        }

        public IActionResult Error() {
            var i = 0;
            var t = 1 / i;
            return Content("error");
        }

        [HttpPost]
        public ActionResult Products(int startpageId, int languageId, List<string> genders, List<int> categories, int? page, bool? noredirect, int currentPageId) {
            Global.SetCulture(languageId);
            var startpage = _umbracoContextFactory.EnsureUmbracoContext().UmbracoContext.Content.GetById(startpageId);
            var currentPage = _umbracoContextFactory.EnsureUmbracoContext().UmbracoContext.Content.GetById(currentPageId);
            var products = WebsiteHelpers.GetProducts(startpage, genders, categories);
            ViewBag.Page = page;
            ViewBag.NoRedirect = noredirect ?? true;
            ViewBag.languageId = languageId;
            ViewBag.CurrentPageUrl = currentPage.Url(mode: UrlMode.Relative);
            var model = products.ToList();
            return Json(new {
                count = products.Count,
                view = this.RenderViewAsync("Partials/_Products", model).Result
            });
            //return View("~/Views/Partials/_Products.cshtml", model);
        }

        //public IActionResult TestDB() {
        //    //_diNa4DataContext.Tests.Add(new Test() {
        //    //    Test1 = "1"
        //    //});
        //    //_diNa4DataContext.SaveChanges();

        //    var contextOptions = new DbContextOptionsBuilder<DiNa4DataContext>()
        //        .UseSqlServer(_configuration.GetConnectionString("umbracoDbDSN"))
        //        //.UseSqlServer("Name=ConnectionStrings:umbracoDbDSN")
        //        .Options;
        //    using (var db = new DiNa4DataContext(contextOptions)) {
        //        db.Tests.Add(new Test() {
        //            Test1 = "2"
        //        });
        //        db.SaveChanges();
        //    }

        //    return Content("test");
        //}

        #region Newsletter
        [HttpPost]
        public ActionResult SendNewsletter(EnquiryPostModel postModel) {
            var result = true;
            //*For MarkupExample see Doku https://docs.google.com/document/d/1-Qywn8wd1VQZ2FRMR6vGKMKoRm58IxHUZakHEaB27OU/edit?usp=drive_web&ouid=106771588122399696377
            string encodedResponse = Request.Form["g-recaptcha-response"];
            var recapchatv3 = new ReCaptchaClassV3Helper(_appSettings);
            if (_appSettings.Value.GoogleRecaptcha.Activate == false
                || (_appSettings.Value.GoogleRecaptcha.Activate && recapchatv3.Validate(encodedResponse, 0.9) == "true")) {
                //result = SaveInDatabase(postModel);
                result = SendNewsletterEmail(postModel);
            }

            return Content(result.ToString());
        }
        #endregion

        [HttpPost]
        public ActionResult SendEnquiry(EnquiryPostModel postModel) {
            var result = false;
            //*For MarkupExample see Doku https://docs.google.com/document/d/1-Qywn8wd1VQZ2FRMR6vGKMKoRm58IxHUZakHEaB27OU/edit?usp=drive_web&ouid=106771588122399696377
            string encodedResponse = Request.Form["g-recaptcha-response"];
            var recapchatv3 = new ReCaptchaClassV3Helper(_appSettings);
            if (_appSettings.Value.GoogleRecaptcha.Activate == false
                || (_appSettings.Value.GoogleRecaptcha.Activate && recapchatv3.Validate(encodedResponse, 0.9) == "true")) {
                //result = SaveInDatabase(postModel);
                result = SendEmail(postModel);
            }

            return Content(result.ToString());
        }

        private bool SaveInDatabase(EnquiryPostModel postModel) {
            var enquiry = new Enquiry() {
                Date = DateTime.Now,
                Firstname = postModel.Firstname,
                Surname = postModel.Surname,
                Ip = postModel.Ip,
                Adults = postModel.Adults,
                Children = postModel.Children,
                ChildrenAge = postModel.ChildrenAges != null ? String.Join(",", postModel.ChildrenAges) : null,
                Nights = postModel.Nights,
                Referer = postModel.Referer,
                Message = postModel.Message,
                Email = postModel.Email,
                Phone = postModel.Phone,
                EnquiryType = 1, // 1 = Direkte Anfrage
                WebsiteId = _appSettings.Value.BoardSettings.WebsiteId,
                LanguageId = postModel.LanguageId,
                Title = postModel.Title,
                Roomcategory = postModel.Room,
                Offer = postModel.Offer,
                //PmsRoomcategoryCode = postModel.Room,
                //PmsRatePlanCode = postModel.Offer,
            };

            if (postModel.Arrival != null) {
                DateTime arrival;
                if (DateTime.TryParseExact(postModel.Arrival, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out arrival)) {
                    enquiry.Arrival = arrival;
                }
            }

            if (postModel.Departure != null) {
                DateTime departure;
                if (DateTime.TryParseExact(postModel.Departure, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out departure)) {
                    enquiry.Departure = departure;
                }
            }

            var supercompanyIds = new List<int>();

            if (_appSettings.Value.BoardSettings.AllowDashboard) {
                supercompanyIds.Add(_appSettings.Value.BoardSettings.SupercompanyId);
            }

            var enquiryApi = new Dina4.EnquiryApi.EnquiryApi();
            var result = enquiryApi.SaveEnquiryWithSupercompanyIds(enquiry, supercompanyIds, _appSettings.Value.BoardSettings.AllowPmsImport ? 1 : 0); // PMS State == 1 == READY FOR PMS IMPORT

            return result;
        }
        private bool SendNewsletterEmail(EnquiryPostModel postModel) {
            string mailAdress = _appSettings.Value.MailSettings.Email_Contact;
            string mailFromAdress = _appSettings.Value.MailSettings.EmailFrom;
            string mailFromName = _appSettings.Value.MailSettings.EmailFromName;
            string hostName = _appSettings.Value.MailSettings.Hostname;
            var context = _umbracoContextFactory.EnsureUmbracoContext().UmbracoContext;

         
            try {
                Global.SetCulture(postModel.LanguageId);
                // Email for Client
                string subject = T.Value("Neue Newsletteranmeldung über ") + hostName;
                var renderedBody = this.RenderViewAsync("Email/NewsletterEmail", postModel).Result;
                string body = renderedBody;
                var mailApi = new MailApi(_configuration.GetConnectionString("enquiry"));
                var mail = new Email() {
                    BodyHtmlText = body, // Wichtig Plaintext funktioniert noch nicht im Service
                    Subject = subject,
                    From = mailFromAdress,
                    FromName = mailFromName,
                    To = mailAdress,
                    ReplyTo = postModel.Email,
                    DateAdded = DateTime.Now // Wird vom SaveMail überschrieben und neu gesetzt.
                };

                mailApi.SaveMail(mail);
            } catch (SmtpException sex) {
                MailHelper.SendErrorMail("Error", "SmtpException: " + sex.Message + "<br/>" + sex.StackTrace);
                return false;
            } catch (Exception ex) {
                MailHelper.SendErrorMail("Error:", ex.Message + "<br/>" + ex.StackTrace);
                return false;
            }

            return true;
        }
        private bool SendEmail(EnquiryPostModel postModel) {
            string mailAdress = _appSettings.Value.MailSettings.Email_Contact;
            string mailFromAdress = _appSettings.Value.MailSettings.EmailFrom;
            string mailFromName = _appSettings.Value.MailSettings.EmailFromName;
            string hostName = _appSettings.Value.MailSettings.Hostname;
            var context = _umbracoContextFactory.EnsureUmbracoContext().UmbracoContext;

            if (postModel.ProductId != null) {
                postModel.ProductName = context.Content.GetById(postModel.ProductId ?? 0).IfNotNull(_ => _.Name);
            }

            if (postModel.Category != null) {
                postModel.CategoryName = context.Content.GetById(postModel.Category ?? 0).IfNotNull(_ => _.Name);
            }

            if (postModel.SubCategory != null) {
                foreach (var x in postModel.SubCategory) {
                    if (postModel.SubCategoryName == null) {
                        postModel.SubCategoryName = new List<string>();
                    }
                    postModel.SubCategoryName.Add(context.Content.GetById(x).IfNotNull(_ => _.Name));

                }
            }

            try {
                Global.SetCulture(postModel.LanguageId);
                // Email for Client
                string subject = T.Value("Neue Anfrage über ") + hostName;
                var renderedBody = this.RenderViewAsync("Email/EmailContact", postModel).Result;
                string body = renderedBody;
                var mailApi = new MailApi(_configuration.GetConnectionString("enquiry"));
                var mail = new Email() {
                    BodyHtmlText = body, // Wichtig Plaintext funktioniert noch nicht im Service
                    Subject = subject,
                    From = mailFromAdress,
                    FromName = mailFromName,
                    To = mailAdress,
                    ReplyTo = postModel.Email,
                    DateAdded = DateTime.Now // Wird vom SaveMail überschrieben und neu gesetzt.
                };

                var attachments = new List<EmailAttachement>();

                if (postModel.Logo1 != null) {
                    using (var ms = new MemoryStream()) {

                        postModel.Logo1.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        attachments.Add(new EmailAttachement() {
                            Attachement = fileBytes,
                            AttachementContentType = postModel.Logo1.ContentType,
                            AttachementName = postModel.Logo1.Name
                        });
                        //mail.Attachement = fileBytes;
                        //mail.AttachementName = postModel.Logo1.Name;
                        //mail.AttachementContentType = postModel.Logo1.ContentType;
                    }
                }

                if (postModel.Logo2 != null) {
                    using (var ms = new MemoryStream()) {

                        postModel.Logo2.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        attachments.Add(new EmailAttachement() {
                            Attachement = fileBytes,
                            AttachementContentType = postModel.Logo2.ContentType,
                            AttachementName = postModel.Logo2.Name
                        });
                    }
                }

                mail.EmailAttachements = attachments;

                mailApi.SaveMail(mail);
            } catch (SmtpException sex) {
                MailHelper.SendErrorMail("Error", "SmtpException: " + sex.Message + "<br/>" + sex.StackTrace);
                return false;
            } catch (Exception ex) {
                MailHelper.SendErrorMail("Error:", ex.Message + "<br/>" + ex.StackTrace);
                return false;
            }

            return true;
        }

        //protected virtual string ToHtml(string viewToRender, ViewDataDictionary viewData) {
        //    var result = ViewEngines.Engines.FindView(ControllerContext, viewToRender, null);

        //    StringWriter output;
        //    using (output = new StringWriter()) {
        //        var viewContext = new ViewContext(ControllerContext, result.View, viewData, ControllerContext.Controller.TempData, output);
        //        result.View.Render(viewContext, output);
        //        result.ViewEngine.ReleaseView(ControllerContext, result.View);
        //    }

        //    return output.ToString();
        //}
    }
}