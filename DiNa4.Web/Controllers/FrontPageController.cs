﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.Extensions.Logging;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web;
using Umbraco.Cms.Web.Common.Controllers;

namespace DiNa4.Web.Controllers {          
    public class FrontPageController : RenderController {
        public FrontPageController(ILogger<RenderController> logger, ICompositeViewEngine compositeViewEngine, IUmbracoContextAccessor umbracoContextAccessor) : base(logger, compositeViewEngine, umbracoContextAccessor) {
        }
        public override IActionResult Index() {
            var test = new ContentResult();
            test.Content = "test";
            return base.Index();// test;

        }

      
    }

}
