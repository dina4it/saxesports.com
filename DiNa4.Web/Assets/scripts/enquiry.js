﻿/*
 * Global variables
 */
var request = null;
/*
 Datepicker Single (e.g. in CTA-Bar)
 */
(function (window, $, undefined) {
    'use strict';

    window.bindDatepicker = function () {
        var $flatpickr = $("#enquiryform-arrival").flatpickr({ locale: website.language, dateFormat: 'd-m-Y', minDate: 'today', allowInput: false, mode: 'range', "plugins": [new rangePlugin({ input: "#enquiryform-departure", allowInput: false })] });
        $("#enquiryform-arrival, #enquiryform-departure").parent().click(function () {
            //console.log('parent clicked');
            $flatpickr.open();
        });

        //var bindDatepicker = function () {
        //    var $flatpickr = $("#arrival").flatpickr({ locale: website.language, dateFormat: 'd-m-Y', minDate: 'today', mode: 'range', "plugins": [new rangePlugin({ input: "#departure" })] });
        //    $("#arrival, #departure").parent().click(function () {
        //        //console.log('parent clicked');
        //        $flatpickr.open();
        //    });
        //};
    };

    window.bindDatepicker();
}(window, $));


/*
 EnquiryForm 
 */
(function (window, $, undefined) {
    'use strict';

    window.showLoading = function () {
        $('body').addClass("loading");
    };

    window.hideLoading = function () {
        $('body').removeClass("loading");
    };

    window.floatLabel = function (inputType) {
        $(inputType).each(function () {
            var $this = $(this);
            // on focus add cladd active to label
            $this.focus(function () {
                $this.siblings('label:not(".error")').addClass("active");
            });

            if ($this.val() != "" && $this.val() != undefined && $this.val() != "blank") {
                $this.siblings('label:not(".error")').addClass("active");
            }

            //on blur check field and remove class if needed
            $this.blur(function () {
                if ($this.val() === '' || $this.val() === 'blank') {
                    $this.siblings('label:not(".error")').removeClass();
                }
            });
        });

        // Fix damit beim Datumauswählen das Label getriggert wird
        $('.datepicker').on('change', function () {
            $('.datepicker').each(function () {
                //$('.').closest('.floatlabel').focus();
                var $this = $(this).closest('.floatlabel');
                if ($this.val() === '' || $this.val() === 'blank') {
                    $this.siblings('label:not(".error")').removeClass();
                } else {
                    $this.siblings('label:not(".error")').addClass("active");
                }
            });
        });
    }

    window.bindEnquiryForm = function () {
        window.floatLabel(".floatlabel");
        var limit = 3;

        $('[name="category"]').on("change", function () {

            var id = $(this).val();
            $('[data-cat]').hide();
            $('[data-cat] input:checked').prop("checked", false);
            $('[data-cat="' + id + '"').show();
        });

        $('[name="colors"]').on("change", function() {
            if ($('[name="colors"]:checked').length == 1) {
                $('#primarycolor').val($('[name="colors"]:checked').val());
            };

            if ($('[name="colors"]:checked').length > limit) {
                this.checked = false;
                return;
            }
        });


        $('#enquiryform').validate({
            ignore: ".ignore",
            submitHandler: function (form) {
                //Save Form Data........
                //var data = $('#enquiryform').serializeObject();
                var f = $('#enquiryform')[0];
                var data = new FormData(f);
                if (request == null) {
                    request = "active";
                    window.showLoading();

                    $.ajax({
                        cache: false,
                        url: "/api/json/sendenquiry",
                        type: "POST",
                        data: data,
                        enctype: 'multipart/form-data',
                        processData: false,  // Important!
                        contentType: false,
                        success: function (callback) {
                            request = null;
                            if (callback == "True") {
                                $('#enquiryform').hide(200);
                                $('#enquiryform-success').removeClass("d-none");
                                $('#enquiryform-error').hide(200);
                            } else {
                                //$('#enquiryform').hide(200);
                                $('#enquiryform-error').removeClass("d-none");
                            }
                            window.hideLoading();
                            if (website.hasTagmanager) {
                                window.gaPushEvent("form", 'enquiry', 'sent');
                            } else {
                               
                            }
                        },
                        error: function (err) {
                            request = null;
                            window.hideLoading();

                            //$('#enquiryform').hide();
                            $('#enquiryform-error').removeClass("d-none");
                        },
                        complete: function () {
                            request = null;
                            window.hideLoading();
                            return false;
                        }
                    });
                    return false;
                }
                return false;
            }
        });

        //jQuery("form#enquiryform").validate({
        //    messages: {
        //        arrival: {
        //            required: $('input[name="arrival"]').data("required")
        //        },
        //        Departure: {
        //            required: $('input[name="departure"]').data("required"),
        //            greaterThan: $('input[name="departure"]').data("greaterThan")
        //        },
        //        firstname: $('input[name="firstname"]').data("required"),
        //        Surname: $('input[name="surname"]').data("required"),
        //        Privacy: $('input[name="privacy"]').data("required"),
        //        Adults: $('input[name="adults"]').data("required"),
        //        Email: {
        //            required: $('input[name="email"]').data("required"),
        //            email: $('input[name="email"]').data("email")
        //        }
        //    }
        //});
    }

    window.bindNewsletterform = function () {

        $('#newsletterform').validate({
            ignore: ".ignore",
            submitHandler: function (form) {
                //Save Form Data........
                var data = $('#newsletterform').serializeObject();
                if (request == null) {
                    request = "active";
                    window.showLoading();
                    $.ajax({
                        cache: false,
                        url: "/api/json/sendnewsletter",
                        type: "POST",
                        data: data,
                        success: function (callback) {
                            request = null;
                            if (callback == "True") {
                                $('#newsletterform').hide(200);
                                $('#newsletterform-success').removeClass("d-none");
                                $('#newsletterform-error').hide(200);
                            } else {
                                //$('#enquiryform').hide(200);
                                $('#newsletterform-error').removeClass("d-none");
                            }
                            window.hideLoading();
                            if (website.hasTagmanager) {
                                window.gaPushEvent("form", 'newsletter-' + website.language, 'sent');
                            } else {

                            }
                        },
                        error: function (err) {
                            request = null;
                            window.hideLoading();
                            $('#newsletterform-error').removeClass("d-none");
                        },
                        complete: function () {
                            request = null;
                            window.hideLoading();
                            return false;
                        }
                    });
                    return false;
                }
                return false;
            }
        });

        jQuery("form#newsletterform").validate({
            messages: {

                Email: {
                    required: $('input[name="email"]').data("required"),
                    email: $('input[name="email"]').data("email")
                }
            }
        });
    }

    window.bindEnquiryForm();
    window.bindNewsletterform();

}(window, $));
