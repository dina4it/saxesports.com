﻿// Passive event listeners
//jQuery.event.special.touchstart = {
//    setup: function (_, ns, handle) { this.addEventListener("touchstart", handle, { passive: !ns.includes("noPreventDefault") }); }
//};
//jQuery.event.special.touchmove = {
//    setup: function (_, ns, handle) { this.addEventListener("touchmove", handle, { passive: !ns.includes("noPreventDefault") }); }
//};

$.fn.serializeObject = function () {
    //https://codereview.stackexchange.com/questions/23491/object-serializer
    var source = this.serializeArray(),
        serialized = {},
        object;
    for (var i = 0; i < source.length; i++) {
        object = source[i];
        if (serialized.hasOwnProperty(object.name)) {
            if (!(serialized[object.name] instanceof Array)) {
                serialized[object.name] = [serialized[this.name]];
            }
            serialized[object.name].push(object.value);
        } else {
            if ($("[name=" + object.name + "]").data("serializearray") != undefined) {
                var arr = new Array();
                arr.push(object.value);
                serialized[object.name] = arr;
            } else {
                if ($("[name=" + object.name + "]").is(":checkbox")) {
                    serialized[object.name] = $("[name=" + object.name + "]").prop("checked");
                } else if ($("[name=" + object.name + "]").is("select")) {
                    serialized[object.name] = $("[name=" + object.name + "] option:selected").val();
                } else {
                    serialized[object.name] = object.value;
                }
            }
        }
    }
    return serialized;
};


/*
 * Set backgrounds.
 */
(function (window, $, undefined) {
    'use strict';

    var updateContainerFunc = function () {
        var result = $('.bgimage').filter(function () {
            if ($.inArray(this, $containers) != -1) {
                return true;
            }
            var respimgval = $(this).attr("data-resp-img");
            var nobg = $(this).attr("data-resp-img-nobg");
            var noinitbg = false;
            var value = this.style.backgroundImage;

            if (respimgval != '' && respimgval != undefined) {
                value = respimgval;
                noinitbg = true;
            }

            if (noinitbg == false && (value == undefined || value.indexOf('url') < 0 || value.indexOf("width") == -1)) {
                return false;
            }

            var url = noinitbg ? value : value.match(urlRegex)[1];
            if (url == undefined) {
                return null;
            }

            var parts = url.split('?');
            var data = {
                file: parts[0],
                parameter: { mode: 'crop', width: 0, height: 0, format: 'jpeg', quality: 84 },
                noinitbg: noinitbg
            };
            parts[1].replace(parameterRegex,
                function (m, k, v) {
                    k = k.toLowerCase();
                    data.parameter[k] = (k === 'width' || k === 'height' || k === 'heightratio') ? parseFloat(v) : v;
                });

            data.parameter.mode = data.parameter.mode || 'crop';
            if (data.parameter.height == undefined &&
                data.parameter.width != undefined &&
                data.parameter.heightratio != undefined) {
                data.parameter.height = data.parameter.width * data.parameter.heightratio;
                delete data.parameter.heightratio;
            }
            if (noinitbg && nobg != "true") {
                var bgcolor = "#efefef";
                var imgcolor = $(this).attr("data-resp-img-bg");
                if (imgcolor != undefined && imgcolor.length > 0) {
                    bgcolor = imgcolor;
                }
                $(this).css('background-color', bgcolor);
            }

            $(this).data('background-data', data);

            return true;
        });
        return result;
    }

    var sizes = [180, 240, 360, 480, 640, 800, 1024, 1200, 1600, 2048],
        urlRegex = new RegExp('[\(\'\"]([^\'\"\)]+)', 'i'),
        parameterRegex = new RegExp('([^=&]+)=([^&]*)', 'ig');

    var $window = $(window),
        $containers = updateContainerFunc(),
        threshold = 50;

    window.updateContainer = function () { $containers = updateContainerFunc(); };

    window.updateBackgrounds = function (resized) {
        var scrollTop = $window.scrollTop(),
            scrollBottom = scrollTop + $window.height();

        $containers.each(function () {
            var $container = $(this),
                containerWidth = $container.width(),
                containerHeight = $container.innerHeight();

            // Ignore containers without image.
            //if ($container.css("background-image").indexOf('url') < 0)
            //    return;

            // Ignore image containers outside viewport.
            var containerTop = $container.offset().top,
                containerBottom = containerTop + containerHeight;
            if (containerTop >= (scrollBottom + threshold) || (containerBottom <= scrollTop - threshold)) {
                return;
            }

            // Get best matching background size.
            var bestWidth = 0,
                bestHeight = 0;

            for (var i = 0; i < sizes.length && bestWidth < containerWidth; i++) {
                bestWidth = sizes[i];
            }

            //for (var j = 0; j < sizes.length && bestHeight < containerHeight; j++)
            //    bestHeight = sizes[j];

            // Break if current images fits.
            var data = $container.data('background-data');
            var rawRatio = data.parameter.height / data.parameter.width;

            bestHeight = bestWidth * rawRatio;

            if ($container.data('resp-img-autosize') == true) {
                bestHeight = 0;
                for (var j = 0; j < sizes.length && bestHeight < containerHeight; j++) {
                    bestHeight = sizes[j];
                }
            }

            bestWidth = 2 * bestWidth;
            bestHeight = 2 * bestHeight;

            if (data.parameter.width === bestWidth && data.parameter.height === bestHeight) {
                if ($container.data('resp-img-autosize') != true && data.noinitbg === true && $container.css('background-image') == 'none') {
                    $container.css('background-image', 'url(' + $(this).attr("data-resp-img") + ')').addClass('loaded');
                }
                // Wenn bereits das richtige Image geladen wurde Aufruf abbrechen
                if ($container.css('background-image') != 'none') {
                    $container.addClass('loaded');
                    return;
                }
            }

            // Update background parameter.
            data.parameter['width'] = bestWidth;
            data.parameter['height'] = bestHeight;
            data.parameter['quality'] = window.featureResult.hasWebp ? 40 : 42;
            if (data.file.length > 0 && data.file.indexOf(".svg") > 0) {
                data.parameter['format'] = 'svg';
            } else if (data.file.length > 0 && data.file.indexOf(".png") > 0) {

                data.parameter['format'] = window.featureResult.hasWebp ? 'webp' : 'png';
            } else {
                data.parameter['format'] = window.featureResult.hasWebp ? 'webp' : 'jpeg';
            }
            $container.data('background-data', data);
            // Build new image URL.
            //if (bestWidth == 0 && bestHeight == 0) {
            //    return;
            //}

            var bestImageUrl = data.file + '?';
            for (var k in data.parameter) {
                bestImageUrl += k + '=' + data.parameter[k] + '&';
            }
            bestImageUrl = bestImageUrl.substring(0, bestImageUrl.length - 1);

            if ($container.data("resp-img-manual") == true) {
                $container.attr("data-resp-img-src", bestImageUrl);
                return;
            }

            // Preload file and set background image.
            var preloadImage = new Image();
            preloadImage.onload = function () {
                $container.css('background-image', 'url(' + this.src + ')');
                $container.addClass('loaded');

                //if ($container.hasClass('content-slider-screen'))
                //    window.initSlideShow();
            };
            preloadImage.src = bestImageUrl;
        });
    };
    $(document).ready(function () {
        window.updateBackgrounds();
        $window.resize();
    });
    $window.resize(window.updateBackgrounds).scroll(window.updateBackgrounds);
}(window, $));

/*
* Set iframe.
    */
(function (window, $, undefined) {
    'use strict';

    var $window = $(window),
        iframecontainer = $('iframe[data-src], img[data-src]'),
        threshold = 50;

    window.updateIframeSrc = function (resized) {
        var scrollTop = $window.scrollTop(),
            scrollBottom = scrollTop + $window.height();

        iframecontainer.each(function () {
            var $container = $(this),
                containerWidth = $container.width(),
                containerHeight = $container.innerHeight();

            // Ignore containers without image.
            //if ($container.css("background-image").indexOf('url') < 0)
            //    return;

            // Ignore image containers outside viewport.
            var containerTop = $container.offset().top,
                containerBottom = containerTop + containerHeight;
            if (containerTop >= (scrollBottom + threshold) || (containerBottom <= scrollTop - threshold)) {
                return;
            }
            if ($container.hasClass("srcset") == false) {
                $container.attr("src", $container.attr("data-src"));
                $container.addClass("srcset");
            }
        });
    };
    $(document).ready(function () { window.updateIframeSrc(); });
    $window.resize(window.updateIframeSrc).scroll(window.updateIframeSrc);
}(window, $));

/*
 * Set Navbar -> todo: wofür? Innerwidth?
 */
(function (window, $, undefined) {
    'use strict';

    var $window = $(window);
    window.minimizeNav = function () {
        if ($('body').data("fixminimize") == undefined || $('body').data("fixminimize") == false) {
            if ($(window).scrollTop() > 0 || $(window).innerWidth() < 1200) {
                $('body').addClass("minnav");
            } else {
                $('body').removeClass("minnav");
            }
        }
    };

    window.minimizeNav();
    $window.scroll(window.minimizeNav);
    $window.resize(window.minimizeNav);
    $('.dropdown-toggle,.navbar-toggle').on('click',
        function () {
            var isopen = $(this).parent('.dropdown.open');
            var fixminnav = $('body').hasClass('fixminnav');
            if (fixminnav == false || isopen.length > 0) {
                $('body').addClass('fixminnav');
            } else {
                $('body').removeClass("fixminnav");
            }
        });
}(window, $));

/*
 * Menutrigger -> Todo: bei welchem Navigation?
 */
(function (window, $, undefined) {
    'use strict';

    var $window = $(window);
    window.menutrigger = function () {
        //$('.menutrigger').on('mouseover',
        //    function () {
        //        var id = $(this).data('menuid');
        //        $('.menu-cont').hide();
        //        $('#menu-' + id).show();
        //    });

        $('.navbar-toggler', 'nav').on('click',
            function () {
                var $that = $(this);
                $('#nav-overlay').toggleClass("visible");
                //$that.toggleClass('collapsed');
                //$('.menutrigger').toggleClass('active');
                $('body').toggleClass('nav-active');
                //$that.parent('nav').find('.navbar-collapse').toggleClass('show');
            });
        if ($('.nav-item-overlay.haschilds.active').length > 0) {
            $('.nav-item-overlay.active').addClass("_active");

            $('.nav-item-overlay.haschilds').on('mouseenter', function () {
                if ($(this) != $('.nav-item-overlay._active')) {
                    $('.nav-item-overlay').removeClass("active");
                }
            });

            $('.nav-item-overlay.haschilds').on('mouseleave', function () {
                $('.nav-item-overlay._active').addClass("active");
            });
        }


    };

    window.menutrigger();
    //$window.scroll(window.minimizeNav);
}(window, $));



(function (window, $, undefined) {
    'use strict';
    function multiselect_deselectAll($el) {
        $('option', $el).each(function (element) {
            $el.multiselect('deselect', $(this).val());
        });
    }
    window.multiselect = function () {
        $('#multiselect1').multiselect({
            numberDisplayed: 4,
            nSelectedText: $('#multiselect1').data('nselectedtext'),
            allSelectedText: $('#multiselect1').data('allselectedtext')
            //buttonText: function (options, select) {
            //    if (options.length == 0) {
            //        return this.nonSelectedText + ' <b class="caret"></b>';
            //    } else {
            //        if (options.length > this.numberDisplayed) {
            //            return options.length + ' ' + this.nSelectedText + ' <b class="caret"></b>';
            //        } else {
            //            var selected = '';
            //            options.each(function () {
            //                var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();

            //                selected += label + ', ';
            //            });
            //            return selected.substr(0, selected.length - 2) + ' <b class="caret"></b>';
            //        }
            //    }
            //}
        });
        $('#multiselect2').multiselect({
            numberDisplayed: 4,
            nSelectedText: $('#multiselect2').data('nselectedtext'),
            allSelectedText: $('#multiselect2').data('allselectedtext')
        });

        $('.js-reset-filter').on('click', function () {
            multiselect_deselectAll($('#multiselect1'));
            multiselect_deselectAll($('#multiselect2'));
            window.updateProducts();
        });
    };

    $(document).ready(function () {
        window.multiselect();
    });
}(window, $));



(function (window, $, undefined) {
    'use strict';
    window.bindCursor = function () {
        if ($(window).innerWidth() > 1200) {
            init_pointer({
                pointerColor: "#CA433F",
                ringSize: 10,
                ringClickSize: 5
            });

            $('[data-cursor], a, .swiper-button-next, .swiper-button-prev, [data-click]').on('mouseenter',
                function () {
                    var cursor = $(this).attr("data-cursor");
                    if (cursor == undefined) {
                        cursor = "hover";
                    }
                    $('#pointer-ring').addClass("on-" + cursor);
                });
            $('[data-cursor], a, .swiper-button-next, .swiper-button-prev, [data-click]').on('mouseleave',
                function () {
                    var cursor = $(this).attr("data-cursor");
                    if (cursor == undefined) {
                        cursor = "hover";
                    }
                    $('#pointer-ring').removeClass("on-" + cursor);
                });
        } else {
            $('#pointer-ring').remove();
            $('#pointer-dot').remove();
        }
    }

    $(document).ready(function () {
        window.bindCursor();
    });

}(window, $));

/*
 * Data Click
 */
(function (window, $, undefined) {
    'use strict';

    window.binddataclick = function () {
        $('div[data-click]').on('click',
            function () {
                var destinationURL = $(this).data('click');
                var target = "";

                if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
                    var referLink = document.createElement('a');
                    referLink.href = destinationURL;
                    referLink.target = target;
                    document.body.appendChild(referLink);
                    referLink.click();
                } else if (target == "_blank") {
                    window.open(destinationURL);
                } else {
                    location.href = destinationURL;
                }
            });
    };

    window.binddataclick();
}(window, $));

/*
 ScrollTo 
 */
(function (window, $, undefined) {
    'use strict';

    window.scrolltoonclick = function () {
        $('.scrollsmooth').on('click',
            function () {
                var hash = $(this).attr('href');
                if (hash != "") {
                    var hashSplitted = hash.split("|");
                    //$.scrollTo($(hash), 1000, { offset: -300 });
                    for (var i = 0; i < hashSplitted.length; i++) {

                        //if (i > 0 && (hash.indexOf("accobook") == -1 || hash.indexOf("package") == -1)) {
                        if (i > 0) {
                            if ($('#' + hashSplitted[i]).length > 0) {
                                $.scrollTo($('#' + hashSplitted[i]), 1000, { offset: -100 });
                            }
                        } else {
                            if ($(hashSplitted[i]).length > 0) {
                                $.scrollTo($(hashSplitted[i]), 1000, { offset: -100 });

                            }
                        }
                    }
                }

                return false;
            });
    };

    window.scrollonload = function () {
        var hash = window.location.hash.substr(1);
        if (hash != "") {
            var hashSplitted = hash.split("|");
            //$.scrollTo($(hash), 1000, { offset: -300 });
            for (var i = 0; i < hashSplitted.length; i++) {

                //if (i > 0 && (hash.indexOf("accobook") == -1 || hash.indexOf("package") == -1)) {
                if (i > 0) {
                    if ($('#' + hashSplitted[i]).length > 0) {
                        $.scrollTo($('#' + hashSplitted[i]), 1000, { offset: -100 });
                    }
                } else {
                    if ($('#' + hashSplitted[i]).length > 0) {
                        $.scrollTo($('#' + hashSplitted[i]), 1000, { offset: -100 });

                    }
                }
            }
        }
    };

    window.scrolltoonclick();

    $(document).ready(function () { window.scrollonload(); });
}(window, $));

/*
 Slickslider 
 */
//(function (window, $, undefined) {
//    'use strict';
//    window.updatePreloadImage = function (container, source) {
//        if (source == undefined) {
//            return;
//        }
//        var $container = $(container);
//        var preloadImage = new Image();
//        preloadImage.onload = function () {
//            $container.css('background-image', 'url(' + this.src + ')');
//            $container.addClass('loaded');
//        };
//        preloadImage.src = source;
//    }

//    window.bindslickslider = function () {
//        var headerslider = $('.headerslider');

//        headerslider.on("afterChange",
//            function (event, slick, currentSlide) {
//                var count = slick.$slides.length;
//                if (currentSlide + 1 <= count) {
//                    var $next = $(slick.$slides.get(currentSlide + 1));
//                    // Load next Image
//                    if ($next != undefined || $next != null) {
//                        window.updatePreloadImage($next, $next.attr("data-resp-img-src"));
//                    }
//                }
//            });

//        headerslider.slick({ autoplay: true, autoplaySpeed: 6000, pauseOnHover: false, dots: false, arrows: false, speed: 500, cssEase: 'linear', fade: true, slide: '.slick-slide' });

//        // Example Slickslider centermode and BackgroundImages
//        /*
//         * ===  Call before init ====
//         * 
//          $('.slidercentermode').on('init',
//            function (slick) {
//                window.updateContainer();
//            });

//            $('.slickslidercentermode').slick(...
//         */
//    };
//    $(document).ready(function () { window.bindslickslider(); });
//}(window, $));

/*
 Swiper 
 */
(function (window, $, undefined) {
    'use strict';


    window.bindswiper = function () {
        var cswiper = new Swiper('.swiper', {
            speed: 400,
            slidesPerView: 1.5,
            spaceBetween: 15,
            infinite: true,
            loop: true,
            centeredSlides: true,
            autoplay: {
                delay: 3000
            },
            navigation: {
                nextEl: '.swiper .swiper-button-next',
                prevEl: '.swiper .swiper-button-prev',
            },
            preloadImages: true,
            // Enable lazy loading
            lazy: {
                enabled: true,
                loadPrevNextAmount: 2,
                loadPrevNext: true
            },

            breakpoints: {
                // when window width is >= 320px
                768: {
                    slidesPerView: 2,
                    spaceBetween: 15
                },
                // when window width is >= 480px
                992: {
                    slidesPerView: 3,
                    spaceBetween: 15
                },
                // when window width is >= 640px
                1200: {
                    slidesPerView: 4,
                    spaceBetween: 15

                }
            }
            //spaceBetween: 100,
        });

        var swiperslider = new Swiper('.swiper-slider', {
            speed: 400,
            slidesPerView: 1,
            infinite: true,
            loop: true,
            centeredSlides: true,
            navigation: {
                nextEl: '.swiper-slider .swiper-button-next',
                prevEl: '.swiper-slider .swiper-button-prev',
            },
            preloadImages: true,
            // Enable lazy loading
            lazy: true,
            on: {
                init: function () {
                    $('.swiper-slider-swipe .swiper-slide').matchHeight();
                    var $slider = $(this.$el);
                    if (this.slides.length > 1) {
                        $slider.find('.swiper-button-next span').html($(this.slides[2]).attr("data-title"));
                        $slider.find('.swiper-button-prev span').html($(this.slides[0]).attr("data-title"));
                    }
                },
                slideChange: function (e) {
                    var $slider = $(this.$el);
                    if (this.slides.length > 1) {
                        var nextId = (this.activeIndex < this.slides.length - 1 ? this.activeIndex + 1 : 0);
                        var prevId = (this.activeIndex <= this.slides.length - 1 && this.activeIndex > 0 ? this.activeIndex - 1 : this.slides.length - 1);
                        $slider.find('.swiper-button-next span').html($(this.slides[nextId]).attr("data-title"));
                        $slider.find('.swiper-button-prev span').html($(this.slides[prevId]).attr("data-title"));
                    }
                }
            }
            //spaceBetween: 100,
        });

        var slider3 = new Swiper('.swiper-slider-swipe', {
            //speed: 400,
            slidesPerView: 1.1,
            spaceBetween: 4,
            //infinite: true,
            //loop: true,
            //centeredSlides: true,
            //navigation: {
            //    nextEl: '.swiper-slider-swipe .swiper-button-next',
            //    prevEl: '.swiper-slider-swipe .swiper-button-prev',
            //},
            //preloadImages: false,
            // Enable lazy loading
            //lazy: true,
            shortSwipes: true,
            longSwipes: true,
            pagination: {
                el: ".swiper-slider-swipe .swiper-pagination",
                clickable: true,
                renderBullet: function (index, className) {
                    return '<span class="' + className + '" data-cursor="clickable"></span>';
                },
            },
            breakpoints: {
                // when window width is >= 320px
                480: {
                    slidesPerView: 1,
                    spaceBetween: 0
                }
            },
            on: {
                init: function () {
                    var $slider = $(this.$el);
                    $slider.find('.act-title').html($(this.slides[0]).attr("data-title"));

                    window.bindCursor();
                },
                slideChange: function (e) {
                    var $slider = $(this.$el);
                    $slider.find('.act-title').html($(this.slides[this.activeIndex]).attr("data-title"));
                }
            }

            //spaceBetween: 100,
        });
    };
    $(document).ready(function () { window.bindswiper(); });
}(window, $));

/*
  Header resize
 */
(function (window, $, undefined) {
    'use strict';
    var lastViewPortWidth = $(window).innerWidth();
    window.resizeHeader = function () {
        if ($(window).innerWidth() != lastViewPortWidth) {
            var headerHeight = $('#header').attr("data-height") / 100;
            var naviheight = document.getElementById("navi");
            var headernull = $('#header').attr("data-height") == 0;
            var h = window.innerHeight * headerHeight.toString().replace(",", ".");
            var body = document.body;
            if ((window.innerHeight > 1100 && window.innerWidth > 1620) || (window.innerHeight > 1100 && window.innerWidth < 1620)) {
                h = window.innerHeight * headerHeight.Replace(",", ".") - document.getElementById("info-header-block").clientHeight;

            }
            if (headernull == false) {
                if (window.innerWidth < 768) {
                    var h = window.innerHeight * 0.5;
                }

                document.getElementById("header").style.height = h + "px";
            };

            lastViewPortWidth = $(window).innerWidth();
        }
    };

    $(window).resize(window.resizeHeader);
}(window, $));

/*
  GA Event Tracker 
 */
(function (window, $, undefined) {
    'use strict';

    window.gaTrackEventUniversal = function () {
        var data = $(this).data('gaq').split('|');
        var cat = data[0];
        var act = data[1];
        var lab = data[2];
        try {
            ga('send', 'event', cat, act, lab);
        } catch (ex) {
        }
    };

    window.gaTrackEvent = function () {
        if (website.hasTagmanager) {
            var data = $(this).data('gaq').split('|');
            var act = data[0];
            var cat = data[1];
            var lab = data[2];
            window.gaPushEvent(act, cat, lab);
        } else {
            var data = $(this).data('gaq').split('|');
            var cat = data[0];
            var act = data[1];
            var lab = data[2];
            try {
                gtag('event', act, { 'event_category': cat, 'event_label': lab });
            } catch (ex) {
            }
        }
    }

    window.gaPushEvent = function (act, cat, lab) {
        try {
            window.dataLayer.push({
                'event': 'ga-ev',
                'ga-ev-cat': cat,
                'ga-ev-act': act,
                'ga-ev-lab': lab
            });
        } catch (ex) {
        }
    }

    $('[data-gaq]').unbind('click', window.gaTrackEvent);
    $('[data-gaq]').on('click', window.gaTrackEvent);
}(window, $));

/* Mobile Menu */
(function (window, $, undefined) {
    'use strict';
    window.mobilemenu = function () {
        $('.nav-item.haschilds').on('click',
            function (e) {
                if (window.innerWidth < 1200) {
                    if ($(e.target).hasClass("nav-link") || $(e.target).parent().hasClass("nav-link")) {
                        if ($(this).hasClass("active")) {
                            $(this).removeClass("active");
                        } else {
                            $('.nav-item.haschilds').removeClass("active");
                            $(this).addClass("active");
                        }
                        return false;
                    }
                }
            });
    };

    $(document).ready(function () {
        window.mobilemenu();
        $('#navi').doubleTapToGo({
            selectorChain: '.haschilds.todoubletap',
            selectorClass: 'doubletap'
        });
    });
}(window, $));

/* Mobile Menu */
(function (window, $, undefined) {
    'use strict';
    window.tabs = function () {
        $('.js-tab').on('click', function () {
            var tabid = $(this).data("id");
            var $container = $(this).closest('.js-tabs');
            $container.find('.js-tab').removeClass("active");
            $container.find('.js-tab-content').removeClass("active");
            $(this).addClass("active");
            $container.find('.js-tab-content[data-id="' + tabid + '"]').addClass("active");
            $(window).resize();
        });
    };

    $(document).ready(function () {
        window.tabs();
    });
}(window, $));


/*
 * Filter update -> Todo: bei welchem Navigation?
 */
(function (window, $, undefined) {
    'use strict';

    var $window = $(window);

    window.updateProducts = function () {
        var data = $('#products-filter').serializeObject();
        data.noredirect = true;
        $.ajax({
            cache: false,
            url: "/api/json/products",
            type: "POST",
            data: data,
            success: function (callback) {
                $('#products-result').html(callback.view);
                window.updateContainer();
                window.updateBackgrounds();
                window.bindPaging();
                $('.js-count').html(callback.count);
                window.bindCursor();
                window.binddataclick();
                var url = new URL(location.protocol + '//' + location.host + location.pathname);

                if (data.categories != undefined) {
                    if (data.categories.length == 1 && $('#products-filter').attr("data-categories") == data.categories[0]) {
                    } else {
                        $(data.categories).each(function () { url.searchParams.append("categories", this); });
                    }
                }
                if (data.genders != undefined) {
                    $(data.genders).each(function () { url.searchParams.append("genders", this); });
                }
                //console.log(url);
                history.replaceState(data, '', url.href);
                //request = null;

                //if (callback == "True") {
                //    $('#newsletterform').hide(200);
                //    $('#newsletterform-success').removeClass("d-none");
                //    $('#newsletterform-error').hide(200);
                //} else {
                //    //$('#enquiryform').hide(200);
                //    $('#newsletterform-error').removeClass("d-none");
                //}
                //window.hideLoading();
            },
            error: function (err) {
                //request = null;
                //window.hideLoading();
            },
            complete: function () {
                //request = null;
                //window.hideLoading();
                return false;
            }
        });
    }

    window.bindPaging = function () {
        $('.js-page').off('click');
        $('.js-page').on('click',
            function () {
                let pageId = $(this).attr('data-page');
                var url = $(this).attr('href');
                $('#products-filter [name="page"]').val(pageId);
                window.updateProducts(pageId);
                //var state = { 'page_id': pageId }
                //history.pushState(state, '', url);
                $.scrollTo($('#products-result'), 1000, { offset: -100 });
                return false;
            });
    };
    window.filterupdate = function () {
        window.bindPaging();

        $('#products-filter select,#products-filter input').on('change',
            function () { window.updateProducts(); });
    };



    window.filterupdate();
    if ($('#products-filter').length > 0) {
        setTimeout(function () { window.updateProducts(); }, 50);
    }
    //$window.scroll(window.minimizeNav);
}(window, $));

//*
//Animations On Scroll
//    * /
(function (window, $, undefined) {
    'use strict';

    window.scrollanimation = function () {
        $(window).scroll(function () { updatePositions(); });
    };

    function updatePositions() {
        var $window = $(window);
        var windowheight = $window.innerHeight();
        var scrollpos = $window.scrollTop();
        var act = scrollpos + windowheight;
        $('.js-scrollleft').each(function () {
            var $that = $(this);

            //var from = $that.offset().top;
            //var to = from + windowheight;
            var p = (act - $that.offset().top) / windowheight;
            //$that.css('transform', "translateX(" + (((100 - (p * 70)) / 2) + 20 + "%") + ")");
            $that.css('transform', "translate3d(" + (((100 - 40 - (p * 80)) / 2) + "%") + ",0,0)");
            //$that.css('left', );
            //$that.css('left', (100 - ((p * 100) * 2))  + "%");
        });
    }

    $(document).ready(function () {
        window.scrollanimation();
        updatePositions();
    });
}(window, $));


/*
  Parallax
*/
(function (window, $, undefined) {
    'use strict';
    /*https://www.jqueryscript.net/animation/background-parallax-scroll-effect.html */
    var $swipertoparallax = $('.headerslider .header-image');



    // Check if the element is in the viewport.
    // http://www.hnldesign.nl/work/code/check-if-element-is-visible/
    function isInViewport(node) {
        // Am I visible? Height and Width are not explicitly necessary in visibility
        // detection, the bottom, right, top and left are the essential checks. If an
        // image is 0x0, it is technically not visible, so it should not be marked as
        // such. That is why either width or height have to be > 0.
        var rect = node.getBoundingClientRect();
        return (
            (rect.height > 0 || rect.width > 0) &&
            rect.bottom >= 0 &&
            rect.right >= 0 &&
            rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.left <= (window.innerWidth || document.documentElement.clientWidth)
        );
    };

    window.parallaxSwiper = function () {
        var scrolled = $(window).scrollTop();
        $swipertoparallax.each(function (index) {
            var $that = $(this);
            //works for slider on top
            $that.css('transform', 'translate3d(0px, ' + parseInt((scrolled * .5)) + 'px, 0px)');
        });

        $(window).scroll(function () {
            var scrolled = $(window).scrollTop();
            $swipertoparallax.each(function (index, element) {
                if (isInViewport(this)) {
                    var $that = $(this);
                    $that.css('transform', 'translate3d(0px, ' + parseInt((scrolled * .5)) + 'px, 0px)');
                }
            });
        });
    };

    $(document).ready(function () {
        window.parallaxSwiper();
    });
}(window, $));


///*
//  Parallax
// */
//(function (window, $, undefined) {
//    'use strict';
//    /*https://www.jqueryscript.net/animation/background-parallax-scroll-effect.html */
//    var $parallaxcontainer = $('.parallax');
//    window.parallax = function () {
//        //var rellax = new Rellax();
//        var scrolled = $(window).scrollTop();
//        $parallaxcontainer.each(function (index) {
//            var $that = $(this);
//            //var imageSrc = $(this).data('image-src');
//            //var imageHeight = $(this).data('height');
//            //$(this).css('background-image', 'url(' + imageSrc + ')');
//            //$(this).css('height', imageHeight);
//            //// Adjust the background position.
//            var initY = $that.offset().top;
//            var height = $that.innerHeight();
//            var diff = scrolled - initY;
//            var ratio = Math.round((diff / height) * 100);
//            $that.css('background-position', 'center ' + parseInt(-(ratio * 1.5)) + 'px');
//        });

//        // Attach scroll event to window. Calculate the scroll ratio of each element
//        // and change the image position with that ratio.
//        // https://codepen.io/lemagus/pen/RWxEYz
//        $(window).scroll(function () {
//            var scrolled = $(window).scrollTop();
//            $parallaxcontainer.each(function (index, element) {
//                var $that = $(this);
//                var initY = $that.offset().top;
//                var height = $that.innerHeight();
//                var endY = initY + $that.innerHeight();

//                // Check if the element is in the viewport.
//                var visible = isInViewport(this);
//                if (visible) {
//                    var diff = scrolled - initY;
//                    var ratio = Math.round((diff / height) * 100);
//                    $that.css('background-position', 'center ' + parseInt(-(ratio * 1.5)) + 'px');
//                }
//            });
//        });

//        // Check if the element is in the viewport.
//        // http://www.hnldesign.nl/work/code/check-if-element-is-visible/
//        function isInViewport(node) {
//            // Am I visible? Height and Width are not explicitly necessary in visibility
//            // detection, the bottom, right, top and left are the essential checks. If an
//            // image is 0x0, it is technically not visible, so it should not be marked as
//            // such. That is why either width or height have to be > 0.
//            var rect = node.getBoundingClientRect();
//            return (
//                (rect.height > 0 || rect.width > 0) &&
//                rect.bottom >= 0 &&
//                rect.right >= 0 &&
//                rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
//                rect.left <= (window.innerWidth || document.documentElement.clientWidth)
//            );

//        };

//    };
//    $(document).ready(function () {
//        window.parallax();
//    });
//}(window, $));

//(function (window, $, undefined) {
//    'use strict';

//    window.initstoerer = function () {
//        var $stoerer = $('#stoererbox');

//        if ($stoerer.length == 0) {
//            return;
//        }

//        var cookieExpires = 7200;

//        var delay = $stoerer.attr("data-delay");
//        var hidemobile = $stoerer.attr("data-hidemobile");
//        if (Cookies.get('stoerer') != 'shown' && (hidemobile == "false" || (hidemobile == "true" && window.innerWidth > 1200))) {
//            setTimeout(function () {
//                //if ($(window).innerWidth() > 1200) {
//                $.fancybox.open({
//                    src: '#stoererbox',
//                    type: 'inline',
//                    opts: {
//                        afterShow: function (instance, current) {
//                            var $img = $('.fancybox-slide #stoererbox img');
//                            $img.attr("src", $img.attr("data-src"));
//                            Cookies.set('stoerer', 'shown', {
//                                expires: cookieExpires, path: '/'
//                            });
//                        }
//                    }
//                });
//                //}
//            }, delay * 1000);
//        }
//    };

//    $(document).ready(function () {
//        window.initstoerer();
//    });
//}(window, $));

/*
  Animate on scroll
 */
(function (window, $, undefined) {
    'use strict';

    window.initaos = function () {
        AOS.init({
            once: true,
            duration: 1000,
            //disable: 'phone'
        });
    };

    $(document).ready(function () {
        window.initaos();
    });
}(window, $));

(function (window, $, undefined) {
    'use strict';

    window.matchheight = function () {
        setTimeout(function () {
            $('.swiper-slider-swipe .swiper-slide').matchHeight();
        }, 200);

    };

    $(document).ready(function () {
        window.matchheight();
    });
}(window, $));