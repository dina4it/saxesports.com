﻿

function Price(elemt) {
    var elemt = elemt;
    var menusize = 0;
    $('[pricenavheight]').each(function () {
        menusize += $(this).innerHeight();
    });

    console.log(menusize);
    var initpos, initheight, initdateheight, initposend;

    var reinit = function () {
        resizeDataCells();
        moveStageToActualCellIndex();
    };


    var cellWidth, numberCellsOnStage, indexActualCell = 0, numberColumns;
    var slidingAnimationSpeed = 1000;
    var init = function () {


        $(document).ready(function () {
            $(elemt).find('.prevBtn').hide();
            resizeDataCells();
            numberColumns = $(elemt).find('.innerstage:first').children().length;

        });

        $(window).resize(function () {
            var initpos = $(elemt).offset().top - menusize;
            var initheight = $(elemt).innerHeight();
            var initdateheight = $(elemt).find('.daterowouter').innerHeight();
            var initposend = initpos + initheight - initdateheight;
            resizeDataCells();
            moveStageToActualCellIndex();
        });

        $(elemt).find('.nextBtn').click(function (event) {
            event.preventDefault();

            $(elemt).find('.innerstage').animate({ marginLeft: '-=' + cellWidth + 'px' }, slidingAnimationSpeed);
            indexActualCell++;

            if (indexActualCell >= (numberColumns - numberCellsOnStage))
                $(this).hide();
            else {
                $(this).show();
            }
            if (indexActualCell > 0) {
                $(elemt).find('.prevBtn').show();
            }

        });
        $(elemt).find('.prevBtn').click(function (event) {
            event.preventDefault();

            $(elemt).find('.innerstage').animate({ marginLeft: '+=' + cellWidth + 'px' }, slidingAnimationSpeed);
            indexActualCell--;

            if (indexActualCell <= 0)
                $(this).hide();
            else {
                $(this).show();
            }
            if (indexActualCell < (numberColumns - numberCellsOnStage)) {
                $(elemt).find('.nextBtn').show();
            }
        });
       

    };

    var moveStageToActualCellIndex = function () {
        //var marginLeft = indexActualCell * cellWidth;
        //$('.innerstage').css('marginLeft', '-' + marginLeft + 'px');
        $(elemt).find('.innerstage').css('marginLeft', '0px');
        indexActualCell = 0;
    };

    var resizeDockedDateBar = function() {
        if ($(window).scrollTop() > initpos) {
            var height = $(elemt).find('.daterowouter').innerHeight();
            $(elemt).find('.daterowouter').addClass('docked');
            // DateRow margintop
            $(elemt).find('.datarowouter').css('margin-top', height);
            $(elemt).find('.dateRow').css('width', $(elemt).innerWidth());
            if (($(window).scrollTop() >= (initposend))) {
                var newpos = menusize - ($(window).scrollTop() - initposend);

                $(elemt).find('.daterowouter').css('top', newpos);
            } else {
                $(elemt).find('.daterowouter').css('top', menusize);
            }
        } else {
            // otherwise put it back in its regular position
            $(elemt).find('.daterowouter').removeClass('docked');
            $(elemt).find('.datarowouter').css('margin-top', 0);
            $(elemt).find('.daterowouter').css('top', 0);
            $(elemt).find('.dateRow').css('width', 'auto');
        }

    }

    var resizeDataCells = function () {
        var stageWidth = $(elemt).find('.stage').width();
        if (stageWidth > 800) {
            numberCellsOnStage = 5;
        } else if (stageWidth > 500) {
            numberCellsOnStage = 3;
        } else if (stageWidth > 400) {
            numberCellsOnStage = 2;
        } else {
            numberCellsOnStage = 1;
        }

        var columns = numberColumns = $(elemt).find('.innerstage:first').children().length;


        if (columns < numberCellsOnStage) {
            numberCellsOnStage = columns;
        }

        cellWidth = stageWidth / numberCellsOnStage;
        $(elemt).find('.dataCell').width(cellWidth);

        moveStageToActualCellIndex();




        if (numberCellsOnStage >= columns) {
            $(elemt).find('.nextBtn').hide();
            $(elemt).find('.prevBtn').hide();
        } else {
            $(elemt).find('.nextBtn').show();
        }

        $(elemt).find('.innerstage .dataCell').each(function () {
            //$(this).height($(this).closest('.dataRow').find('.firstColumn').height());
            //$(this).height($(this).closest('.dataRow').find('.firstColumn').height());
            $(this).css('line-height', ($(this).closest('.dataRow').find('.firstColumn').height() - 8) + 'px'); // 10 == 5*2 Padding
        });


        
        initpos = $(elemt).offset().top - menusize;
        initheight = $(elemt).innerHeight();
        initdateheight = $(elemt).find('.daterowouter').innerHeight();
        initposend = initpos + initheight - initdateheight;

        resizeDockedDateBar();

        $(document).on('scroll',
            function () {
                resizeDockedDateBar();


            });

    }

    return { init: init, reinit: reinit };

}