﻿/*
 * Set feature tests.
 */
(function (document, element, undefined) {
    'use strict';

    (window.runFeatureTests = function () {
        window.featureResult = {};

        function setFeatureResult(name, isValid) {
            element.className = element.className.replace(new RegExp('(^|\\s)(no-)?' + name + '(\\s|$)'), '$1$3') + (isValid ? ' ' : ' no-') + name;
            window.featureResult['has' + name.charAt(0).toUpperCase() + name.slice(1).toLowerCase()] = isValid;
        }

        setFeatureResult('js', true);
        setFeatureResult('cookiesusage', document.cookie.indexOf('acceptedcookiesusage=1') >= 0);
        setFeatureResult('placeholder', 'placeholder' in HTMLInputElement.prototype);

        var canvas = document.createElement('canvas'), mt = 'image/webp';
        setFeatureResult('webp', canvas.toDataURL && canvas.toDataURL(mt).indexOf(mt) >= 0);

        var mediaQuery = '(-webkit-min-device-pixel-ratio: 1.5),(min--moz-device-pixel-ratio: 1.5),(-o-min-device-pixel-ratio: 3/2),(min-resolution: 1.5dppx)';
        setFeatureResult('hidpi', window.devicePixelRatio > 1 || (window.matchMedia && window.matchMedia(mediaQuery).matches));
    })();

}(document, document.documentElement));
