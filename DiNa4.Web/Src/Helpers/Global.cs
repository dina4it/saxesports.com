﻿using System.Globalization;
using System.Threading;
using Dina4.Web.src;

namespace DiNa4.Web.Helpers {
    public class Global {
        public static Language GetLanguage() {
            var lang = Thread.CurrentThread.CurrentUICulture.Name;
            switch (lang) {
                default:
                case "de": return Language.De;
                case "it": return Language.It;
                case "en": return Language.En;
            }
        }

        public static string GetCulture() {
            var lang = Thread.CurrentThread.CurrentUICulture.Name;
            return lang;
        }

        public static void SetCulture(int langId) {
            switch (langId) {
                default:
                case (int)Language.De:
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("de");
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("de");
                    break;
                case (int)Language.It:
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("it");
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("it");
                    break;
                case (int)Language.En:
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en");
                    break;
            }
        }
    }
}
