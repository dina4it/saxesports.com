﻿using System.Collections.Generic;
using System.Linq;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Web.Common.PublishedModels;
using Umbraco.Extensions;

namespace DiNa4.Web.Helpers {
    public static class WebsiteHelpers {
        public static List<string> BlockListViewsCache = new();
        public static string ParseTextareaToHtml(string value) { return value.IfNotNull(_ => _.Replace("\n", "<br>")); }

        public static string GetBackgroundImageCropUrlWidthTag(this IPublishedContent content, int width, int height, int quality = 40) {
            return $"background-image: url({content.GetCropUrl(width: width, height: height, quality: quality)});";
        }

        public static FrontPage StartPage(this IPublishedContent content) { return (FrontPage)content.AncestorOrSelf(1); }

        public static List<ProductColor> ProductColors(this IPublishedContent content) {
            return content.AncestorOrSelf(1).Siblings().FirstOrDefault(_ => _.ContentType.Alias == Umbraco.Cms.Web.Common.PublishedModels.ProductColors.ModelTypeAlias)?.Children.Select(_ => (ProductColor)_).ToList();
        }

        public static List<IPublishedContent> GetProducts(IPublishedContent startpage, List<string> genders, List<int> categories) {
            List<IPublishedContent> products = new List<IPublishedContent>();

            if (categories != null && categories.Any()) {
                products = startpage?.Descendants().Where(_ => categories.Contains(_.Id)).SelectMany(_ => _.Descendants().Where(__ => __.ContentType.Alias == ProductPage.ModelTypeAlias)).ToList();
            } else {
                products = startpage?.Descendants().Where(_ => _.ContentType.Alias == ProductPage.ModelTypeAlias).ToList();
            }

            if (genders != null && genders.Any()) {
                // Unisex immer dazurechnen

                //genders.Add("Unisex");
                products = products.Where(_ => genders.ContainsAny(_.Value<string[]>("gender"))).ToList();
            }
            return products;
        }

        //public static IEnumerable<string> GetPrevaluesFromDataType(string name) {
        //    var serviceData = UmbracoContext.Current.Application.Services.DataTypeService;
        //    var datatype = serviceData.GetDataTypeDefinitionByName(name);   // 1245 = Data type Id
        //    var values = serviceData.GetPreValuesByDataTypeId(datatype.Id);   // 1245 = Data type Id
        //    return values;
        //}
    }
} 