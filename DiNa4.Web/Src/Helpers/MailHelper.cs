﻿using System;
using System.Net.Mail;
using System.Text;
using Microsoft.Extensions.Options;
using Umbraco.Cms.Core.Configuration.Models;

namespace DiNa4.Web.Helpers {
    public class MailHelper {
        //private static ILogger _logger;
        private static IOptions<GlobalSettings> _globalSettings;
        private static IOptions<AppSettings> _appSettings;

        public static void Configure(IOptions<AppSettings> appSettings, IOptions<GlobalSettings> globalSettings) {

            _appSettings = appSettings;
            _globalSettings = globalSettings;
        }

        public static void SendMail(string mailTo, string mailFrom, string mailFromName, string mailSubject, string mailBody, string replyTo) {
            try {
                var message = new MailMessage();
                message.IsBodyHtml = true;

                message.From = new MailAddress(mailFrom, mailFromName);
                message.To.Add(new MailAddress(mailTo));
                if (!String.IsNullOrEmpty(replyTo)) {
                    message.ReplyTo = new MailAddress(replyTo);
                }
                message.Subject = mailSubject;
                message.Body = mailBody;
                message.BodyEncoding = Encoding.GetEncoding("utf-8");
                var client = new SmtpClient();
                client.Send(message);
                message.Dispose();
            } catch (Exception ex) {
                try {
                    //Try sending without replyTo set
                    SendMail(mailTo, mailFrom, mailFromName, mailSubject, mailBody, "");
                } catch (Exception ex1) {
                    SendErrorMail(_appSettings?.Value.MailSettings.EmailAdmin, "MailHelper Send error with " + mailTo + "<br>" + ex.Message + "<br>" + ex.StackTrace);
                }
            }
        }

        public static void SendMail(string mailTo, string mailFrom, string mailFromName, string mailSubject,
            string mailBody) {
            try {
                var message = new MailMessage();
                message.IsBodyHtml = true;

                message.From = new MailAddress(mailFrom, mailFromName);
                message.To.Add(new MailAddress(mailTo));

                message.Subject = mailSubject;
                message.Body = mailBody;
                message.BodyEncoding = Encoding.GetEncoding("utf-8");
                var client = new SmtpClient(_globalSettings.Value.Smtp.Host);
                client.Send(message);
                message.Dispose();
                //_logger.LogInformation("Email sent to " + mailTo);
            } catch (Exception ex) {
                //_logger.LogError(ex, ex.Message);
            }
        }

        public static void SendErrorMail(string mailSubject, string mailBody) {
            var recipients = _appSettings?.Value.MailSettings.EmailAdmin ?? "admin@dina4.it";
            var mailFrom = _appSettings?.Value.MailSettings.EmailFrom ?? "mail@dina4.net";
            var mailFromName = _appSettings?.Value.MailSettings.EmailFromName ?? "Website DINA4";
            SendMail(recipients, mailFrom, mailFromName, mailSubject, mailBody);
        }

        public static void SendErrorMail(string mailSubject, Exception ex, string mailFromName) {
            SendMail(_appSettings?.Value.MailSettings.EmailAdmin, _appSettings?.Value.MailSettings.EmailFrom, mailFromName, mailSubject,
                ex.Message + "<br>" + ex.StackTrace);
        }
    }
}