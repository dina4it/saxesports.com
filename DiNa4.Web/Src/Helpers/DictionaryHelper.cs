﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.AspNetCore.Hosting;
using Umbraco.Cms.Core.Services;
using Umbraco.Extensions;

namespace DiNa4.Web.Helpers {
    public class T {
        private static ILocalizationService _localizationService;
        private static IWebHostEnvironment _hostEnvironment;
        private const string DefaultParentKey = "_Frontend";

        public static void Configure(ILocalizationService localizationService, IWebHostEnvironment hostEnvironment) {
            _localizationService = localizationService;
            _hostEnvironment = hostEnvironment;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key">Dictionary Key</param>
        /// <param name="parent">Parent Key Seperated with . --- Is only used last parent</param>
        /// <param name="culture">override culture --- use for example: de-DE</param>
        /// <returns></returns>
        public static string Value(string key, string parent = null, string culture = null) {
            var localizationService = _localizationService;

            if (String.IsNullOrEmpty(culture)) {
                culture = Thread.CurrentThread.CurrentUICulture.Name;
            }

            var dictionary = localizationService.GetDictionaryItemByKey(key);
            if (dictionary != null) {
                var culturelang = dictionary.Translations.FirstOrDefault(_ => _.Language.CultureInfo.ToString() == culture);
                if (culturelang != null) {
                    if (String.IsNullOrEmpty(culturelang.Value)) {
                        return key;
                    }

                    return culturelang.Value;
                }

                return key;
            }

            // Create New DictionaryEntry
            // Get Parent Folder 
            Guid? parentId = null;
            if (String.IsNullOrEmpty(parent)) {
                parent = DefaultParentKey;
            }
            if (!String.IsNullOrEmpty(parent)) {
                var folderParsed = parent.Split('.');
                // only lastfolder used
                foreach (var x in folderParsed) {
                    var folderentry = localizationService.GetDictionaryItemByKey(x);
                    if (folderentry == null) {
                        var item = localizationService.CreateDictionaryItemWithIdentity(x, parentId, null);
                        parentId = item.Key;
                    } else {
                        parentId = folderentry.Key;
                    }
                }
            }

            localizationService.CreateDictionaryItemWithIdentity(key, parentId, null);

            return key;
        }

        /// <summary>
        /// Adds or update an existing Dictionary Item
        /// </summary>
        /// <param name="key"></param>
        /// <param name="valueDe"></param>
        /// <param name="valueIt"></param>
        /// <param name="valueEn"></param>
        /// <param name="parent">Creates Folders seperated with . at example. Folder1.Folder2</param>
        public static void AddOrUpdate(string key, string valueDe, string valueIt, string valueEn, string parent = null, bool force = false) {
            var localizationService = _localizationService;
            var dictionary = localizationService.GetDictionaryItemByKey(key);

            // Create New DictionaryEntry
            // Get Parent Folder 
            Guid? parentId = null;
            if (!String.IsNullOrEmpty(parent)) {
                var folderParsed = parent.Split('.');
                // only lastfolder used
                foreach (var x in folderParsed) {
                    var folderentry = localizationService.GetDictionaryItemByKey(x);
                    if (folderentry == null) {
                        var item = localizationService.CreateDictionaryItemWithIdentity(x, parentId, null);
                        parentId = item.Key;
                    } else {
                        parentId = folderentry.Key;
                    }
                }
            }

            // Only update new
            if (dictionary == null && force == false) {
                var newitem = localizationService.CreateDictionaryItemWithIdentity(key, parentId, null);

                localizationService.AddOrUpdateDictionaryValue(newitem, localizationService.GetLanguageByIsoCode("de"), valueDe);
                localizationService.AddOrUpdateDictionaryValue(newitem, localizationService.GetLanguageByIsoCode("it"), valueIt);
                localizationService.AddOrUpdateDictionaryValue(newitem, localizationService.GetLanguageByIsoCode("en"), valueEn);
                localizationService.Save(newitem);
            } else {
                localizationService.AddOrUpdateDictionaryValue(dictionary, localizationService.GetLanguageByIsoCode("de"), valueDe);
                localizationService.AddOrUpdateDictionaryValue(dictionary, localizationService.GetLanguageByIsoCode("it"), valueIt);
                localizationService.AddOrUpdateDictionaryValue(dictionary, localizationService.GetLanguageByIsoCode("en"), valueEn);
                localizationService.Save(dictionary);
            }
        }

        //private static List<DictionaryItem> _dictionaryItems = new List<DictionaryItem>();
        public static bool ImportCsv() {
            string fullPath = Path.Combine(_hostEnvironment.WebRootPath, "ExportDictionary.csv");// _mediaFileSystem.GetFullPath(path);
            var i = 0;
            foreach (string line in System.IO.File.ReadLines(fullPath)) {
                if (i == 0) {
                    i++;
                    continue;
                }
                System.Console.WriteLine(line);
                var split = line.Split(";");
                if (split.Length >= 5) {
                    AddOrUpdate(split[1], split[2], split[3], split[4], split[0], true);
                }

                i++;
            }

            return true;
        }
        public static string ExportAllToCsv() {
            //For now, we're only going to get the English items
            List<List<string>> result = new List<List<string>>();

            var root = _localizationService.GetRootDictionaryItems();

            if (!root.Any()) {
                return null;
            }
            result.Add(new List<string>() {
                "ParentKey",
                "ItemKey",
                "DE",
                "It",
                "En"
            });
            foreach (var item in root) {
                List<string> parentKey = new List<string>();
                var p = item;
                while (p.ParentId != null) {
                    p = _localizationService.GetDictionaryItemById(p.ParentId.Value);
                    parentKey.Add(p.ItemKey);
                }

                parentKey.Reverse();

                result.Add(new List<string>() {
                    String.Join(".", parentKey),
                    item.ItemKey,
                    item.Translations.FirstOrDefault(_ => _.Language.CultureInfo.ToString() == "de").IfNotNull(__ => __.Value),
                    item.Translations.FirstOrDefault(_ => _.Language.CultureInfo.ToString() == "it").IfNotNull(__ => __.Value),
                    item.Translations.FirstOrDefault(_ => _.Language.CultureInfo.ToString() == "en").IfNotNull(__ => __.Value),
                });
                var childs = LoopThroughDictionaryChildren(item.Key);
                if (childs != null) {
                    result.AddRange(childs);
                }
            }

            return String.Join(Environment.NewLine, result.Select(__ => String.Join(";", __)));
        }

        private static List<List<string>> LoopThroughDictionaryChildren(Guid key) {
            var items = _localizationService.GetDictionaryItemChildren(key);
            List<List<string>> result = new List<List<string>>();
            if (!items.Any()) {
                return null;
            }

            foreach (var subItem in items) {
                List<string> parentKey = new List<string>();

                var p = subItem;
                while (p.ParentId != null) {
                    p = _localizationService.GetDictionaryItemById(p.ParentId.Value);
                    parentKey.Add(p.ItemKey);
                }

                parentKey.Reverse();
                result.Add(new List<string>() {
                    String.Join(".", parentKey),
                    subItem.ItemKey,
                    subItem.Translations.FirstOrDefault(_ => _.Language.CultureInfo.ToString() == "de-DE").IfNotNull(__ => __.Value),
                    subItem.Translations.FirstOrDefault(_ => _.Language.CultureInfo.ToString() == "it-IT").IfNotNull(__ => __.Value),
                    subItem.Translations.FirstOrDefault(_ => _.Language.CultureInfo.ToString() == "en-GB").IfNotNull(__ => __.Value),
                });

                var childs = LoopThroughDictionaryChildren(subItem.Key);
                if (childs != null) {
                    result.AddRange(childs);
                }
            }

            return result;
        }
    }
}