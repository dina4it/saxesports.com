﻿using System.Collections.Generic;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace DiNa4.Web.Helpers {
    public class ReCaptchaClassV3Helper {
        private readonly IOptions<AppSettings> _appSettings;

        public ReCaptchaClassV3Helper(IOptions<AppSettings> appSettings) { _appSettings = appSettings; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="encodedResponse"></param>
        /// <param name="minscore">0-1</param>
        /// <returns></returns>
        public string Validate(string encodedResponse, double minscore) {
            var client = new System.Net.WebClient();
            string privateKey = _appSettings.Value.GoogleRecaptcha.PrivateKey;
            var googleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", privateKey, encodedResponse));

            var captchaResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ReCaptchaClassV3Helper>(googleReply);
            if (captchaResponse.Score < minscore) {
                return "false";
            }

            return captchaResponse.Success.ToLower();
        }

        [JsonProperty("success")]
        public string Success {
            get { return m_Success; }
            set { m_Success = value; }
        }

        private string m_Success;

        [JsonProperty("error-codes")]
        public List<string> ErrorCodes {
            get { return m_ErrorCodes; }
            set { m_ErrorCodes = value; }
        }

        private List<string> m_ErrorCodes;

        [JsonProperty("score")]
        public double Score {
            get { return score; }
            set { score = value; }
        }

        private double score;
    }
}