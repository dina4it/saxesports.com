﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dina4.Web.src;
using Umbraco.Cms.Core.Cache;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.Strings;
using Umbraco.Extensions;

namespace Dina4.Web.Helpers {
    public static class WebsiteUrlHelper {

        private static IShortStringHelper _shortStringHelper;
        public static void Configure(IShortStringHelper shortStringHelper) {
        }


        public static string PackageUrl(string name, int id, Language languageId) {
            switch (languageId) {
                case Language.De:
                    return $"/de/angebot/{name.ToSafeFileName(_shortStringHelper)}/{id}";
                case Language.It:
                    return $"/it/offerta/{name.ToSafeFileName(_shortStringHelper)}/{id}";
                case Language.En:
                    return $"/en/offer/{name.ToSafeFileName(_shortStringHelper)}/{id}";
            }
            return "";
        }


        /// <summary>
        /// https://24days.in/umbraco-cms/2020/multilingual-websites-in-umbraco-8/
        /// Generates the list of languages for the page dropdown. If there is a parallel page using same culture then this is used, otherwise it falls back to the root URL.
        /// </summary>
        /// <param name="currentPage">The currently viewed page</param>
        /// <param name="currentCulture">The current culture</param>
        /// <returns>A list of languages and cultures</returns>
        public static List<CultureUrl> GetCurrentLanguageList(this IPublishedContent currentPage, string currentCulture, AppCaches caches) {
            var home = currentPage.Root();
            //var rootCultures = GetRootCultures(home, caches);
            //Order current culture first
            var rootCultures = GetRootCultures(home, caches).OrderByDescending(_ => _.Culture.InvariantEquals(currentCulture));
            var pageCultures = currentPage.Cultures.Select(x => x.Value);
            var mapped = new List<CultureUrl>();

            foreach (var root in rootCultures) {
                var pageMatch = pageCultures.FirstOrDefault(c => c.Culture == root.Culture);
                CultureUrl mappedPage = null;

                if (pageMatch != null) {
                    string url = currentPage.Url(pageMatch.Culture);

                    if (url != "#") {
                        mappedPage = new CultureUrl() {
                            Culture = pageMatch.Culture,
                            Url = url
                        };
                    }
                }

                if (mappedPage == null) {
                    mappedPage = new CultureUrl() {
                        Culture = root.Culture,
                        Url = home.Url(root.Culture)
                    };
                }

                mappedPage.Current = mappedPage.Culture.InvariantEquals(currentCulture);
                mapped.Add(mappedPage);
            }

            return mapped;
        }

        private static IEnumerable<PublishedCultureInfo> GetRootCultures(IPublishedContent currentPage, AppCaches caches) {
            return caches.RuntimeCache.GetCacheItem("RootCultures", () => currentPage.Cultures.Select(x => x.Value).ToList());
        }

        public class CultureUrl {
            /// <summary>
            /// The culture string eg. en-GB
            /// </summary>
            public string Culture { get; set; }

            /// <summary>
            /// The page URL
            /// </summary>
            public string Url { get; set; }

            /// <summary>
            /// Gets the short name eg. GB
            /// </summary>
            public string Name => this.Culture?.Length > 2 ? this.Culture?.Substring(0, 2).ToUpper() : this.Culture?.ToUpper();

            /// <summary>
            /// Gets whether this is the currently selected page
            /// </summary>
            public bool Current { get; set; }
        }
    }
}