﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dina4.Web.src;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web.Common.ApplicationBuilder;
using Umbraco.Extensions;

namespace DiNa4.Web.Middleware {

    public class ExternalReferrerParserMiddleware {
        private readonly RequestDelegate _next;
        private readonly IUmbracoContextAccessor _umbracoContextAccessor;
        private List<string> _domains;

        public ExternalReferrerParserMiddleware(RequestDelegate next, IUmbracoContextAccessor umbracoContextAccessor) {
            _next = next;
            _umbracoContextAccessor = umbracoContextAccessor;
        }

        public async Task Invoke(HttpContext context) {

            if (_domains == null) {
                _domains = _umbracoContextAccessor.GetRequiredUmbracoContext().Domains.GetAll(false).Select(x => x.Name.Replace("www.", "")).ToList();
            }

            string referrer;
            if (TryParse(context.Request, out referrer)) {
                try {
                    // 'external' Referer
                    if (context.Session.Keys.Any(_ => _ == AppSettings.EXTERNAL_REFERER) == false) {
                        context.Session.SetString(AppSettings.EXTERNAL_REFERER, referrer);
                    }
                } catch { }
            }

            await _next.Invoke(context);
            // Clean up.
        }

        public bool TryParse(HttpRequest request, out string referrer) {
            referrer = null;
            var refererFromHeader = request.GetTypedHeaders().Referer;
            if (refererFromHeader != null) {
                var referrerhost = refererFromHeader.Host.Replace("www.", "");
                var isReferrerExternal = true;
                foreach (var domain in _domains) {
                    if (domain.Contains(referrerhost)) {
                        isReferrerExternal = false;
                        break;
                    }
                }

                // External Referer
                if (isReferrerExternal) {
                    referrer = refererFromHeader.ToString();
                }
            }
            return !string.IsNullOrEmpty(referrer);
        }
    }

    public static class ExternalReferrerParserExtensions {
        public static IUmbracoApplicationBuilderContext UseExternalReferrerParserMiddleware(this IUmbracoApplicationBuilderContext builder) {

            builder.AppBuilder.UseMiddleware<ExternalReferrerParserMiddleware>();
            return builder;

        }
    }
}