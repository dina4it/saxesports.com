﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DiNa4.Web.Helpers;
using Dina4.Web.src;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Umbraco.Cms.Core.Routing;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web.Common.ApplicationBuilder;
using Umbraco.Cms.Web.Common.PublishedModels;
using Umbraco.Extensions;
using Language = Dina4.Web.src.Language;

namespace DiNa4.Web.Middleware {
    public class LanguageRedirectMiddleware {

        private const Language DefaultLanguage = Language.De;
        public static List<Domain> AllLanguages = null;
        public static List<string> AllCultures = null;
        private readonly RequestDelegate _next;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly IUmbracoContextAccessor _umbracoContextAccessor;
        private readonly IContentService _contentService;

        public LanguageRedirectMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings, IUmbracoContextAccessor umbracoContextAccessor, IContentService contentService) {
            _next = next;
            _appSettings = appSettings;
            _umbracoContextAccessor = umbracoContextAccessor;
            _contentService = contentService;
        }

        public async Task Invoke(HttpContext context) {
            //Do something with context near the beginning of request processing.
            // Load first time languages when UmbracoContext is presented
            if (AllLanguages == null) {
                AllLanguages = _umbracoContextAccessor.GetRequiredUmbracoContext().Domains.GetAll(false).ToList();
                AllCultures = _contentService?.GetRootContent().FirstOrDefault(_ => _.ContentType.Alias == FrontPage.ModelTypeAlias)?.PublishedCultures.ToList();
            }

            if (_appSettings?.Value.AllowLanguageRedirect == true) {
                // Nur CMS Seiten erlauben diese Funktion zu benutzen
                if (context.Request.Path.Value == null || context.Request.Path.Value.Contains(".")) {
                    return;
                }

                var lang = context.Request.Cookies["lang"];

                if (context.Request.Path.Value.Length <= 1) {
                    if (lang != null) {
                        SetLanguageInCookieAndRedirect((Language)Enum.Parse(typeof(Language), lang));
                    }

                    var acceptedLanguages = context.Request.GetTypedHeaders().AcceptLanguage.Select(_ => _.Value.Value);
                    if (acceptedLanguages == null) {
                        SetLanguageInCookieAndRedirect(Language.En);
                    } else {
                        var isSet = false;
                        foreach (var x in acceptedLanguages) {
                            if (x.Contains("de") && WebsiteContainsLanguage("de")) {
                                SetLanguageInCookieAndRedirect(Language.De);
                                isSet = true;
                                break;
                            }

                            if (x.Contains("it") && WebsiteContainsLanguage("it")) {
                                SetLanguageInCookieAndRedirect(Language.It);
                                isSet = true;
                                break;
                            }

                            if (x.Contains("en") && WebsiteContainsLanguage("en")) {
                                SetLanguageInCookieAndRedirect(Language.En);
                                isSet = true;
                                break;
                            }
                        }

                        if (!isSet) {
                            SetLanguageInCookieAndRedirect(DefaultLanguage);
                        }
                    }
                }

                var parsedLang = ParseLangByUrl(context.Request.Host + context.Request.Path);

                if (lang == null || (lang != null && parsedLang.ToString() != lang)) {
                    SetOrUpdateLangCookie(parsedLang);
                }
            }

            bool WebsiteContainsLanguage(string lang) { return AllCultures.Any(_ => _.Contains(lang)); }

            Language ParseLangByUrl(string absoluteUri) {
                foreach (var x in AllLanguages) {
                    if (absoluteUri.StartsWith(x.Name.Trim("https://").Trim("http://"))) {
                        if (x.Culture.Contains("de")) {
                            return Language.De;
                        }

                        if (x.Culture.Contains("it")) {
                            return Language.It;
                        }

                        if (x.Culture.Contains("en")) {
                            return Language.En;
                        }
                    }
                }

                return DefaultLanguage;
            }

            void SetLanguageInCookieAndRedirect(Language language) {
                var startPage = AllLanguages.Where(x => x.Name.Contains(context.Request.Host.Value)).FirstOrDefault(x => x.Culture.Contains((language).ToString().ToLower()));

                if (startPage != null) {
                    var page = _umbracoContextAccessor.GetRequiredUmbracoContext().Content.GetById(startPage.ContentId);
                    if (page == null) {
                        return;
                    }

                    SetOrUpdateLangCookie(language);

                    var querystring = context.Request.QueryString.ToString();
                    if (startPage.Name.Contains("http")) {
                        context.Response.Redirect(startPage.Name + (!String.IsNullOrEmpty(querystring) ? "?" + querystring : ""));
                    } else {
                        try {
                            var newuri = new Uri(context.Request.Scheme + @"://" + startPage.Name);
                            context.Response.StatusCode = 302;
                            if (context.Response.Headers.ContainsKey("Location")) {
                                context.Response.Headers["Location"] = newuri.AbsolutePath + (!String.IsNullOrEmpty(querystring) ? "?" + querystring : "");
                            } else {
                                context.Response.Headers.Add("Location", newuri.AbsolutePath + (!String.IsNullOrEmpty(querystring) ? "?" + querystring : ""));
                            }
                        } catch (Exception ex) {
                            MailHelper.SendErrorMail("Error Languageredict", (ex.Message + ex.StackTrace + " language:" + language.ToString() + " " + page.Url()));
                        }
                    }

                    //context.Response.End();
                }
            }

            void SetOrUpdateLangCookie(Language language) {
                var domain = context.Request.IfNotNull(_ => _.Host.Value);
                domain = Regex.Replace(domain, @"^www\.", ".");

                var response = context.Response;
                response.Cookies.Delete("lang");
                context.Response.Cookies.Append("lang",
                    language.ToString(),
                    new Microsoft.AspNetCore.Http.CookieOptions() {
                        Path = domain
                    });
            }

            await _next.Invoke(context);

            // Clean up.
        }
    }

    public static class LanguageRedirectExtensions {

        public static IUmbracoApplicationBuilderContext UseLanguageRedirectMiddleware(this IUmbracoApplicationBuilderContext builder) {

            builder.AppBuilder.UseMiddleware<LanguageRedirectMiddleware>();
            return builder;

        }
    }
}