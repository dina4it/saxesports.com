﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Notifications;
using Umbraco.Cms.Core.PropertyEditors.ValueConverters;
using Umbraco.Extensions;
//using ImageProcessor;
//using ImageProcessor.Imaging;

namespace DiNa4.Web.Notifications {
    public class ImagePreprocessingHandler : INotificationHandler<MediaSavingNotification> {
        private readonly IOptions<AppSettings> _appSettings;
        private readonly IWebHostEnvironment _hostEnvironment;

        public ImagePreprocessingHandler(IOptions<AppSettings> appSettings, IWebHostEnvironment hostEnvironment) {
            _appSettings = appSettings;
            _hostEnvironment = hostEnvironment;
        }
        public void Handle(MediaSavingNotification notification) {
            if (_appSettings.Value.NConvert.Activate == true) {
                Save(notification.SavedEntities);
            }
            // do stuff
        }

        private void Save(IEnumerable<IMedia> notificationSavedEntities) {
            IEnumerable<string> supportedTypes = new List<string>() { "jpg", "png", "jpeg" };// _contentSection.ImageFileTypes.ToList();}
            // If MediaType Image
            foreach (IMedia media in notificationSavedEntities) {
                if (media.HasProperty("umbracoFile") && media.ContentType.Alias == Umbraco.Cms.Web.Common.PublishedModels.Image.ModelTypeAlias) {
                    //Make sure it's an image.
                    string cropInfo = media.GetValue<string>("umbracoFile");
                    string path = cropInfo;
                    if (cropInfo.Contains("src")) {
                        path = JsonConvert.DeserializeObject<ImageCropperValue>(cropInfo).Src;
                    }

                    if (path != media.GetValue<string>("filename")) {

                        string extension = Path.GetExtension(path).Substring(1);

                        // JPEG TIFF usw. excluded but svg and png
                        if (supportedTypes.InvariantContains(extension) && extension != "svg" && extension != "png") {
                            string fullPath = Path.Combine(_hostEnvironment.WebRootPath, path.TrimStart("/")); // _mediaFileSystem.GetFullPath(path);
                            ManipulateImageWithNConvert(fullPath);

                            using (var image = Image.FromFile(fullPath)) {
                                FileInfo fi = new FileInfo(fullPath);
                                media.SetValue("umbracoWidth", image.Width);
                                media.SetValue("umbracoHeight", image.Height);
                                media.SetValue("umbracoBytes", fi.Length);
                            }
                        }

                        // PNG
                        if (extension == "png") {
                            string fullPath = Path.Combine(_hostEnvironment.WebRootPath, path.TrimStart("/"));
                            ManipulateImageWithPngGuantAndNConvert(fullPath);

                            using (var image = Image.FromFile(fullPath)) {
                                FileInfo fi = new FileInfo(fullPath);
                                media.SetValue("umbracoWidth", image.Width);
                                media.SetValue("umbracoHeight", image.Height);
                                media.SetValue("umbracoBytes", fi.Length);
                            }
                        }
                        media.SetValue("filename", path);
                    }
                }
            }
        }

        private void ManipulateImageWithPngGuantAndNConvert(string fullPath) {
            System.Diagnostics.ProcessStartInfo proc = new System.Diagnostics.ProcessStartInfo();
            System.Diagnostics.ProcessStartInfo proc2 = new System.Diagnostics.ProcessStartInfo();
            var nconvertPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NConvert");
            var pngguantPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Pngquant");
            proc.FileName = pngguantPath + "\\pngquant.exe";
            proc2.FileName = nconvertPath + "\\nconvert.exe";
            var args = String.Format(@"--force --ext .png --verbose 256 ""{0}""", fullPath);
            var args2 = String.Format(@"-ratio -quiet -rtype lanczos -rflag decr -resize {0} {1} -D -overwrite -out png -o ""{2}"" ""{2}""",
                _appSettings.Value.NConvert.MaxWidth, _appSettings.Value.NConvert.MaxHeight, fullPath);
            proc.Arguments = args;
            proc2.Arguments = args2;
            System.Diagnostics.Process process2 = System.Diagnostics.Process.Start(proc2);
            process2.WaitForExit();
            System.Diagnostics.Process process = System.Diagnostics.Process.Start(proc);
            process.WaitForExit();
        }
        private void ManipulateImageWithNConvert(string fullPath) {
            System.Diagnostics.ProcessStartInfo proc = new System.Diagnostics.ProcessStartInfo();
            System.Diagnostics.ProcessStartInfo proc2 = new System.Diagnostics.ProcessStartInfo();
            var nconvertPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NConvert");
            var pathForSrgb = nconvertPath + "\\sRGB2014.icc";
            proc.FileName = nconvertPath + "\\nconvert.exe";
            proc2.FileName = nconvertPath + "\\nconvert.exe";
            var args = String.Format(@"-jpegtrans exif -ratio -quiet -rtype lanczos -rflag decr -resize {0} {1} -icc_intent 0 -icc_in """" -icc_out ""{4}"" -q {2} -D -overwrite -out jpeg -o ""{3}"" ""{3}""",
                _appSettings.Value.NConvert.MaxWidth, _appSettings.Value.NConvert.MaxHeight, _appSettings.Value.NConvert.Quality, fullPath, pathForSrgb);
            var args2 = String.Format(@"-ratio -quiet -rtype lanczos -rflag decr -resize {0} {1} -icc_intent 0 -icc_in """" -icc_out ""{4}"" -q {2} -D -overwrite -out jpeg -o ""{3}"" ""{3}""",
                _appSettings.Value.NConvert.MaxWidth, _appSettings.Value.NConvert.MaxHeight, _appSettings.Value.NConvert.Quality, fullPath, pathForSrgb);
            proc.Arguments = args;
            proc2.Arguments = args2;
            System.Diagnostics.Process process = System.Diagnostics.Process.Start(proc);
            process.WaitForExit();
            System.Diagnostics.Process process2 = System.Diagnostics.Process.Start(proc2);
            process2.WaitForExit();
        }

    }
}