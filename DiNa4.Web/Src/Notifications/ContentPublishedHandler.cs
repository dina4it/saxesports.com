﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Umbraco.Cms.Core.Composing;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Notifications;
using Umbraco.Cms.Core.Services.Implement;

namespace DiNa4.Web.Notifications {

    public class ContentPublishedNotificationHandler : INotificationHandler<ContentPublishedNotification> {
        private readonly ILogger<ContentPublishedNotificationHandler> _logger;

        public ContentPublishedNotificationHandler(ILogger<ContentPublishedNotificationHandler> logger) { _logger = logger; }

        public void Handle(ContentPublishedNotification notification) {
            //do stuff
        }
    }
}