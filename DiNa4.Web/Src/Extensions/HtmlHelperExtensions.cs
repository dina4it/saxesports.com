﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Web;
using Dina4.Web.src;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Web.Common.ApplicationBuilder;
using Umbraco.Extensions;

namespace Dina4.Web.Extensions {
    /// <summary>
    /// HtmlHelper extenions used for rendering  nestedContents.
    /// </summary>
    public static class HtmlHelperExtensions {
        private static IHttpContextAccessor _httpContextAccessor;

        public static void Configure(IHttpContextAccessor httpContextAccessor) { _httpContextAccessor = httpContextAccessor; }

        /// <summary>
        /// Renders the partials based on the model, partial path and given viewdata dictionary.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="nestedContents">The nestedContents model.</param>
        /// <param name="partialPath">The partial path.</param>
        /// <param name="viewDataDictionary">The view data dictionary.</param>
        /// <returns></returns>
        private static HtmlString _renderPartials(this IHtmlHelper htmlHelper, IEnumerable<IPublishedElement> nestedContents, string partialPath, ViewDataDictionary viewDataDictionary) {
            var sb = new StringBuilder();
            if (nestedContents == null) {
                return new HtmlString("");
            }

            int i = 0;
            foreach (var fieldsetModel in nestedContents) {
                i++;
                //if (viewDataDictionary == null) {
                //    viewDataDictionary = new ViewDataDictionary() { { "Index", i } };
                //} else {
                //    if (viewDataDictionary.ContainsKey("Index")) {
                //        viewDataDictionary["Index"] = i;
                //    } else {
                //        viewDataDictionary.Add("Index", i);
                //    }
                //}
                sb.AppendLine(_renderPartial(htmlHelper, fieldsetModel, partialPath, viewDataDictionary).ToString());
            }

            return new HtmlString(sb.ToString());
        }

        /// <summary>
        /// Renders the fieldset partial
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="fieldsetModel"></param>
        /// <param name="partialPath">The partial path.</param>
        /// <param name="viewDataDictionary">The view data dictionary.</param>
        /// <returns></returns>
        private static HtmlString _renderPartial(this IHtmlHelper htmlHelper, IPublishedElement nestedContent, string partialPath, ViewDataDictionary viewDataDictionary) {
            var context = _httpContextAccessor.HttpContext;

            if (nestedContent == null || context == null) {
                return new HtmlString("");
            }

            //var sb = new StringBuilder();

            //default
            var pathToPartials = "Views/NestedContent/";

            if (!string.IsNullOrEmpty(partialPath)) {
                pathToPartials = partialPath;
            }

            var partial = string.Format("{0}{1}.cshtml", pathToPartials, nestedContent.ContentType.Alias);

            if (System.IO.File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, partial))) {
                var partialOutput = htmlHelper.PartialAsync("~/" + partial, nestedContent, viewDataDictionary).GetAwaiter().GetResult();
                using (var writer = new System.IO.StringWriter()) {
                    partialOutput.WriteTo(writer, HtmlEncoder.Default);
                    return new HtmlString(writer.ToString());
                }
            } else {
                //Current.Logger.Info<IPublishedContent>(string.Format("The partial for {0} could not be found.  Please create a partial with that name or rename your alias.", context.Server.MapPath(partial)));
            }

            return null;
        }

        /// <summary>
        /// Renders a single archtype partial from a fieldset
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="fieldsetModel">The fieldset model.</param>
        /// <returns></returns>
        public static HtmlString RenderNestedContentPartial(this IHtmlHelper htmlHelper, IPublishedContent fieldsetModel) { return _renderPartial(htmlHelper, fieldsetModel, null, null); }

        /// <summary>
        /// Renders the nestedContents partial.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="fieldsetModel">The fieldset model.</param>
        /// <param name="partialView">The partial view.</param>
        /// <returns></returns>
        public static HtmlString RenderNestedContentPartial(this IHtmlHelper htmlHelper, IPublishedContent fieldsetModel, string partialView) {
            return _renderPartial(htmlHelper, fieldsetModel, partialView, null);
        }

        /// <summary>
        /// Renders the nestedContents partial.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="fieldsetModel">The fieldset model.</param>
        /// <param name="partialView">The partial view.</param>
        /// <param name="viewDataDictionary">The view data dictionary.</param>
        /// <returns></returns>
        public static HtmlString RenderNestedContentPartial(this IHtmlHelper htmlHelper, IPublishedContent fieldsetModel, string partialView, ViewDataDictionary viewDataDictionary) {
            return _renderPartial(htmlHelper, fieldsetModel, partialView, viewDataDictionary);
        }

        /// <summary>
        /// Renders the nestedContents partials.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="nestedContents">The nestedContents model.</param>
        /// <returns></returns>
        public static HtmlString RenderNestedContentPartials(this IHtmlHelper htmlHelper, IPublishedElement model, string alias) {
            var nestedContents = model.Value<List<IPublishedElement>>(alias);
            return _renderPartials(htmlHelper, nestedContents, null, null);
        }

        /// <summary>
        /// Renders the nestedContents partials.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="nestedContents">The nestedContents model.</param>
        /// <returns></returns>
        public static HtmlString RenderNestedContentPartials(this IHtmlHelper htmlHelper, IEnumerable<IPublishedElement> nestedContents) {
            return _renderPartials(htmlHelper, nestedContents, null, null);
        }

        /// <summary>
        /// Renders the nestedContents partials.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="nestedContents">The nestedContents model.</param>
        /// <param name="partialPath">The partial path.</param>
        /// <returns></returns>
        public static HtmlString RenderNestedContentPartials(this IHtmlHelper htmlHelper, IEnumerable<IPublishedElement> nestedContents, string partialPath) {
            return _renderPartials(htmlHelper, nestedContents, partialPath, null);
        }

        /// <summary>
        /// Renders the nestedContents partials.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="nestedContents">The nestedContents model.</param>
        /// <param name="viewDataDictionary">The view data dictionary.</param>
        /// <returns></returns>
        public static HtmlString RenderNestedContentPartials(this IHtmlHelper htmlHelper, IEnumerable<IPublishedElement> nestedContents, ViewDataDictionary viewDataDictionary) {
            return _renderPartials(htmlHelper, nestedContents, null, viewDataDictionary);
        }

        /// <summary>
        /// Renders the nestedContents partials.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="nestedContents">The nestedContents model.</param>
        /// <param name="partialPath">The partial path.</param>
        /// <param name="viewDataDictionary">The view data dictionary.</param>
        /// <returns></returns>
        public static HtmlString RenderNestedContentPartials(this IHtmlHelper htmlHelper, IEnumerable<IPublishedElement> nestedContents, string partialPath, ViewDataDictionary viewDataDictionary) {
            return htmlHelper._renderPartials(nestedContents, partialPath, viewDataDictionary);
        }
    }
}