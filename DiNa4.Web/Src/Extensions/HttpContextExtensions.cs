﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DiNa4.Web.Extensions {
    public static class HttpContextExtensions {
        public static string GetCurrentDomainWithSchema(this HttpContext context) {
            var request = context.Request;
            return request.Scheme + "://" + request.Host;
        }

        public static string UserAgent(this HttpContext context) { return context.Request.Headers["User-Agent"]; }

        public static string GetIpAddress(this HttpContext context) {
            string ipAddress = context?.Connection?.RemoteIpAddress?.ToString();
            return ipAddress;
        }
    }
}