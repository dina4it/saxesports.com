﻿using System;
using System.Text.RegularExpressions;
using Lucene.Net.Util.Automaton;
using Umbraco.Cms.Core.Strings;
using Umbraco.Extensions;

namespace DiNa4.Web {
    public static class StringExtensions {
        public static string PrepareForHeading(this string value) {
            if (value == null) {
                return null;
            }

            var parsedValue = StripP(value);
            return parsedValue;
        }

        public static IHtmlEncodedString PrepareForHeading(this IHtmlEncodedString value) {
            if (value == null) {
                return null;
            }

            var parsedValue = StripP(value.ToHtmlString());

            return new HtmlEncodedString(parsedValue);
        }

        private static string StripP(string value) {
            value = value.TrimEnd().TrimEnd("</p>");
            value = value.Replace("</p>", "<br>");
            value = Regex.Replace(value, "<p[^>,.]*>", "");
            return value;
        }
    }
}
