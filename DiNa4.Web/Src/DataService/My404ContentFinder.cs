﻿using System.Linq;
using System.Threading.Tasks;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.Routing;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web.Common.PublishedModels;

namespace DiNa4.Web.DataService {
    public class My404ContentFinder : IContentLastChanceFinder {
        private readonly IDomainService _domainService;
        private readonly IUmbracoContextAccessor _umbracoContextAccessor;

        public My404ContentFinder(IDomainService domainService, IUmbracoContextAccessor umbracoContextAccessor) {
            _domainService = domainService;
            _umbracoContextAccessor = umbracoContextAccessor;
        }

        public Task<bool> TryFindContent(IPublishedRequestBuilder contentRequest) {
            // Find the root node with a matching domain to the incoming request
            var allDomains = _domainService.GetAll(true).ToList();
            var domain = allDomains?
                .FirstOrDefault(f => contentRequest.Uri.ToString().Contains(f.DomainName));
            //f.DomainName == contentRequest.Uri.Authority
            //                         || f.DomainName == $"https://{contentRequest.Uri.Authority}"
            //                         || f.DomainName == $"http://{contentRequest.Uri.Authority}");

            var siteId = domain != null ? domain.RootContentId : allDomains.Any() ? allDomains.FirstOrDefault()?.RootContentId : null;

            if (!_umbracoContextAccessor.TryGetUmbracoContext(out var umbracoContext)) {
                return Task.FromResult(false); // false;
            }
            var siteRoot = umbracoContext.Content.GetById(false, siteId ?? -1);

            if (siteRoot is null) {
                return Task.FromResult(false);
            }

            // Assuming the 404 page is in the root of the language site with alias fourOhFourPageAlias
            var notFoundNode = siteRoot.Children.FirstOrDefault(f => f.ContentType.Alias == Page404.ModelTypeAlias);

            if (notFoundNode is not null) {
                contentRequest.SetPublishedContent(notFoundNode);
            }

            // Return true or false depending on whether our custom 404 page was found
            return Task.FromResult(contentRequest.PublishedContent is not null);
        }
    }
}
