/// <binding ProjectOpened='watch' />
/// <vs AfterBuild='defaultcss' SolutionOpened='watch:css,watch:scripts, defaultcss' />
module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: { // sass tasks
            site: {
                options: {
                    //compass: true, // enable the combass lib, more on this later
                    style: 'expanded' // we don't want to compress it
                },
                files: {
                    'assets/css/site.css': 'assets/css/sass/site.scss' // this is our main scss file
                }
            },
            sitecritical: {
                options: {
                    //compass: true, // enable the combass lib, more on this later
                    style: 'expanded' // we don't want to compress it
                },
                files: {
                    'assets/css/site_critical_css.css': 'assets/css/sass/site_critical.scss' // this is our main scss file
                }
            },
            bootstrap: {
                options: {
                    //compass: true, // enable the combass lib, more on this later
                    style: 'expanded' // we don't want to compress it
                },
                files: {
                    'assets/css/bootstrap.css': 'assets/css/bootstrap4/bootstrap.scss' // this is our main scss file
                }
            }
        },
        cssmin: {
            compress: {
                files: {
                    'wwwroot/assets/css/site.min.css': [
                        //'content/css/bootstrap4/bootstrap.css',
                        //'content/css/bootstrap.css',
                        'assets/css/site.css',
                        //'assets/scripts/slickslider/slick.css',
                        'assets/scripts/fancybox/jquery.fancybox.css',
                        'assets/scripts/flatpickr/flatpickr.css',
                        'assets/scripts/swiper/swiper-bundle.min.css',
                        'assets/scripts/pointerjs/pointer.css',
                        'assets/scripts/multiselect/dist/css/bootstrap-multiselect.min.css',
                    ]
                }
            },
            critical: {
                options: {
                    keepSpecialComments: 0
                },
                files: {
                    'wwwroot/assets/css/site_critical_css.min.css': [
                        'assets/css/bootstrap.css',
                        'assets/scripts/aos/aos.css',

                        //'content/css/bootstrap.css',
                        'assets/css/site_critical_css.css'
                    ]
                }
            }
        },
        postcss: {
            options: {
                //map: true, // inline sourcemaps

                //// or
                //map: {
                //    inline: false, // save all sourcemaps as separate files...
                //    annotation: 'content/css/maps/' // ...to the specified directory
                //},

                processors: [
                    require('pixrem')(), // add fallbacks for rem units
                    require('autoprefixer')({ browsers: 'last 2 versions' }), // add vendor prefixes
                    require('cssnano')() // minify the result
                ]
            },
            dist: {
                src: 'assets/css/site.css'
            }
        },
        // Watch
        watch: {
            sass: {
                files: ['assets/css/sass/*.scss'],
                tasks: ['defaultsass'],
                // tasks: ['sass', 'autoprefixer'],
                options: {
                    spawn: false
                }
            },
            scripts: {
                files: ['assets/scripts/*.js'],
                tasks: ['defaultjs'],
                options: {
                    spawn: false,
                },
            },
        },
        uglify: {
            sitejs: {
                files: {
                    'wwwroot/assets/scripts/site.min.js': [
                        'assets/scripts/jquery-3.6.0.min.js',
                        //'content/scripts/jquery/smoothScroll.js',
                        //'assets/scripts/bootstrap.min.js',
                        'assets/scripts/bootstrap.bundle.min.js',
                        //'assets/scripts/slickslider/slick.min.js',
                        //'content/scripts/price.js',
                        'assets/scripts/jquery.scrollTo.min.js',
                        'assets/scripts/fancybox/jquery.fancybox.js',
                        'assets/scripts/jquery.dcd.doubletaptogo.js',
                        'assets/scripts/swiper/swiper-bundle.min.js',
                        'assets/scripts/multiselect/dist/js/bootstrap-multiselect.min.js',
                        'assets/scripts/pointerjs/pointer.js',
                        'assets/scripts/jquery.matchHeight-min.js',
                        'assets/scripts/aos/aos.js',
                        //'content/scripts/readmore.min.js',
                        'assets/scripts/footer.js'
                    ],
                    'wwwroot/assets/scripts/header.min.js': [
                        'assets/scripts/header.js'
                    ],
                    'wwwroot/assets/scripts/swiper.min.js': [
                        'assets/scripts/swiper-bundle.js'
                    ],
                    'wwwroot/assets/scripts/enquiry.min.js': [
                        'assets/scripts/jquery.validate.De.It.En.js',
                        'assets/scripts/flatpickr/flatpickr.min.js',
                        'assets/scripts/flatpickr/rangePlugin.js',
                        'assets/scripts/flatpickr/l10n/de.js',
                        'assets/scripts/flatpickr/l10n/it.js',
                        'assets/scripts/flatpickr/l10n/default.js',
                        'assets/scripts/enquiry.js'
                    ]
                }
            }
        },
        clean: ["wwwroot/lib/*", "temp/"],
    });

    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks('grunt-contrib-sass'); 
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-postcss');

    //grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');

    grunt.registerTask('defaultsass', ['sass:site', 'sass:sitecritical', 'postcss', 'cssmin']);
    grunt.registerTask('defaultjs', ['uglify']);
};