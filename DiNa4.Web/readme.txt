========== Umbraco Upgrade =====================
CMD unter Dina4.Web
dotnet add package Umbraco.Cms --version
dotnet add package Umbraco.Cms.SqlCe --version
dotnet restore
 "Unattended": {
                "UpgradeUnattended": true
            }


== Debuggen Hostnamen ändern für IIS Binding ==
\DiNa4.Web\Properties\launchSettings.json Hostnamen anpassen


=========== Entity Framework ====================         


Install-Package Microsoft.EntityFrameworkCore.Tools 5er version da .net 5
Install-Package Microsoft.EntityFrameworkCore.SqlServer 5er version da .net 5

== DB CODE Generieren
Scaffold-DbContext "Name=ConnectionStrings:umbracoDbDSN" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Src/DB -Tables "test","test2" -ContextDir Src/DB -Context DiNa4DataContext -Namespace DiNa4.Web.DB -ContextNamespace DiNa4.Web.DB -f

========== Fehlermeldung ======================== 
-> ERROR: location for package 'System.Security.Cryptography.Pkcs missing dann paket installieren
   SOLUTION: Install-Package System.Security.Cryptography.Pkcs -Version 6.0.0

========== Online stellen
- Projekt in einen Ordern publishen und online stellen
- Danach einfach CSS vom wwwroot und die bins von Bin Ordner hinaufkopieren

========== File in wwwroot automatisch kopieren nach Builden
- in csproj z.b. 
<ItemGroup>
      <Content Include="Content\img\**" Link="..\..\..\wwwroot\assets\img\%(RecursiveDir)%(Filename)%(Extension)" CopyToOutputDirectory="PreserveNewest" />
      <Content Include="Content\fonts\**" Link="..\..\..\wwwroot\assets\fonts\%(RecursiveDir)%(Filename)%(Extension)" CopyToOutputDirectory="PreserveNewest" />
      <Content Include="content\css\rte.css" Link="..\..\..\wwwroot\assets\css\%(Filename)%(Extension)" CopyToOutputDirectory="PreserveNewest" />
      <Content Include="content\css\site_critical_css.css.map" Link="..\..\..\wwwroot\assets\css\%(Filename)%(Extension)" CopyToOutputDirectory="PreserveNewest" />
      <Content Include="content\css\site.css.map" Link="..\..\..\wwwroot\assets\css\%(Filename)%(Extension)" CopyToOutputDirectory="PreserveNewest" />
</ItemGroup>